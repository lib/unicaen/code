<?php

/**
 * @var $this       \Application\View\Renderer\PhpRenderer
 * @var $container  \Psr\Container\ContainerInterface
 */

$cg = \UnicaenCode\Util::codeGenerator();
$inspec = \UnicaenCode\Util::introspection();

$modules = $inspec->getModules();

foreach( $modules as $module ){
    $cg->generer('templateMap', ['module' => $module['name']]);
    $cg->generer('autoloadClassmap', ['module' => $module['name']]);
}

$cg->generer('applicationPhpRenderer');