<?php

/**
 * @var $this       \Application\View\Renderer\PhpRenderer
 * @var $container  \Psr\Container\ContainerInterface
 */

$data = [];

$dir = getcwd();
$name = $container->get('config')['unicaen-app']['app_infos']['nom'];
$data[$dir] = countLignes($dir, 'Application '.$name);

$unicaenLibs = scandir($dir.'/vendor/unicaen');
unset($unicaenLibs[0]);unset($unicaenLibs[1]); // pas de . et ..

foreach ($unicaenLibs as $lib) {
    $dir = getcwd().'/vendor/unicaen/'.$lib;
    $data[$dir] = countLignes($dir, 'unicaen/'.$lib);
}

?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Nombre de fichiers</th>
        <th>Nombre de lignes de code</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach( $data as $d): ?>
    <tr>
        <td><?= $d['name'] ?></td>
        <td><?= $d['fichiers'] ?></td>
        <td><?= $d['lignes'] ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php


function countLignes(string $dir, string $name): array
{
    $ignored = [
        $dir . '/vendor',
        $dir . '/public/vendor',
        $dir . '/public/ace-builds-master', // pour Unicaen/import
        $dir . '/.idea',
        $dir . '/cache',
        $dir . '/log',
        $dir . '/.git',
        $dir . '/node_modules',
        $dir . '/package-lock.json',
        $dir . '/composer.lock',
        $dir . '/deploy.log',
        $dir . '/autoload_classmap.php',
    ];

    $res = parseCode($dir, $ignored);

    $res['name'] = $name;
    return $res;
}


function parseCode($dir, $ignored = [])
{
    $count = [
        'fichiers' => 0,
        'lignes'   => 0,
    ];
    $allowedExts = [
        'php', 'json', 'js', 'css', 'xml', 'phtml', 'dist', 'sql', 'yml', 'po', 'htaccess', 'txt', 'html',
    ];

    $i = scandir($dir);

    foreach ($i as $fd) {
        if (!in_array($fd, ['.', '..'])) {
            $item = $dir . '/' . $fd;
            if (!in_array($item, $ignored)) {
                if (is_file($item)) {
                    $ext = strtolower(substr($item, strrpos($item, '.') + 1));
                    if (in_array($ext, $allowedExts)) {
                        $count['fichiers'] += 1;
                        $count['lignes'] += parseFileLines($item);
                    }
                } elseif (is_dir($item)) {
                    $res = parseCode($item, $ignored);
                    $count['fichiers'] += $res['fichiers'];
                    $count['lignes'] += $res['lignes'];
                }
            }
        }
    }

    return $count;
}

function parseFileLines($filename)
{
    $res = 0;
    $lines = file($filename);
    foreach ($lines as $line) {
        if ('' != trim($line)) {
            $res++;
        } // on supprime les lignes vides
    }

    return $res;
}
