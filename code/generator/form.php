<?php

use UnicaenCode\Util;

return [
    'title'          => 'Création d\'un nouveau formulaire',
    'generator'      => function (array $params): array {
        $class       = $params['class'] ?? null;
        $type        = $params['type'] ?? Util::classType($class) ?? 'Form';
        $author      = $params['author'] ?? Util::getAuthor();
        $template    = $params['template'] ?? 'Form';
        $useHydrator = $params['useHydrator'] ?? true;

        $namespace = Util::classNamespace($class);
        $classname = Util::classClassname($class);
        $filename  = Util::classFilename($class);

        $params['namespace']   = $namespace;
        $params['classname']   = $classname;
        $params['author']      = $author;
        $params['template']    = $template;
        $params['type']        = $type;
        $params['useHydrator'] = $useHydrator;
        $params['filename']    = $filename;
        $params['config']      = true;

        return $params;
    },
    'type'           => [
        'type'    => 'select',
        'label'   => 'Type de formulaire (Form ou Fieldset)',
        'options' => ['Form' => 'Form', 'Fieldset' => 'Fieldset'],
        'value'   => 'Form',
    ],
    'class'          => [
        'label' => 'Nom de classe du formulaire',
        'value' => 'Application\Form\MonFormulaireForm',
    ],
    'useHydrator'    => [
        'type'  => 'checkbox',
        'label' => 'Implémenter un hydrateur spécifique',
        'value' => false,
    ],
    'awareTrait'     => [
        'type'  => 'checkbox',
        'label' => 'Générer un trait',
        'value' => true,
    ],
    'awareInterface' => [
        'type'  => 'checkbox',
        'label' => 'Générer une interface',
        'value' => false,
    ],
    'useGetter'      => [
        'type'  => 'checkbox',
        'label' => 'Générer des getters dans les traits et les interfaces',
        'value' => true,
    ],
    'factory'        => [
        'type'  => 'checkbox',
        'label' => 'Générer une factory',
        'value' => true,
    ],
    'subDir'         => [
        'type'  => 'checkbox',
        'label' => 'Les traits, interfaces et Factory seront placés dans des sous-dossiers dédiés',
        'value' => false,
    ],
];