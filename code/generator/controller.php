<?php

use UnicaenCode\Generator\ControllerConfig;
use UnicaenCode\Util;

return [
    'title' => 'Création d\'un nouveau contrôleur',

    'class' => [
        'label' => 'Nom de classe du contrôleur',
        'value' => 'Application\Controller\ExempleController',
    ],

    'route' => [
        'label' => 'Route par laquelle y accéder depuis une URL (en minus-cules ou snake_case)',
        'value' => 'exemple-de-controlleur',
    ],

    'factory' => [
        'type'  => 'checkbox',
        'label' => 'Générer une factory',
        'value' => true,
    ],

    'subDir' => [
        'type'  => 'checkbox',
        'label' => 'Les traits, interfaces et Factory seront placés dans des sous-dossiers dédiés',
        'value' => false,
    ],

    'generator' => function (array $params): array {
        $class    = $params['class'] ?? null;
        $type     = $params['type'] ?? Util::classType($class) ?? 'Controller';
        $author   = $params['author'] ?? Util::getAuthor();
        $template = $params['template'] ?? 'Controller';
        $subDir   = $params['subDir'] ?? false;

        $namespace = Util::classNamespace($class);
        $classname = Util::classClassname($class);
        $filename  = Util::classFilename($class);

        $params['class']     = $class;
        $params['namespace'] = $namespace;
        $params['classname'] = $classname;
        $params['author']    = $author;
        $params['template']  = $template;
        $params['type']      = $type;
        $params['filename']  = $filename;
        $params['controllerConfig'] = true;

        Util::codeGenerator()->generateFrom($params, 'factory');
        Util::codeGenerator()->generateFrom($params, 'controllerConfig');

        if (isset($params['factory']['class'])) {
            $params['controllerConfig']['factoryClass'] = Util::classTruncated($params['factory']['class'], Util::classModule($class));
        } else {
            $params['controllerConfig']['factoryClass'] = Util::classTruncated($class, Util::classModule($class));
        }

        return $params;
    },
];