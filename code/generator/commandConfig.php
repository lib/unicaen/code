<?php

use UnicaenCode\Util;

return [
    'title'   => 'Génération de configuration de commande console',
    'class'   => [
        'label' => 'Classe de la console',
        'value' => 'Application\Command\ExempleCommand',
    ],
    'name'    => [
        'label' => 'Nom de la commande',
        'value' => 'application:exemple',
    ],
    'factory' => [
        'label' => 'Classe de la factory correspondante',
        'value' => 'Application\Service\ExempleServiceFactory',
    ],

    'generator' => function (array $params): array {
        $class    = $params['class'] ?? null;
        $template = $params['template'] ?? 'CommandConfig';

        $namespace = Util::classModule($class, Util::CLASS_MODULE_NAMESPACE);
        $filename = Util::classModule($class, Util::CLASS_MODULE_CONFIG)['relPath'] . '/config/module.config.php';

        $category     = 'services';
        $factoryClass = $params['factory']['class'] ?? $class;

        $params = [
            'factoinvo'    => 'factories',
            'namespace'    => $namespace,
            'name'         => $params['name'],
            'class'        => Util::classTruncated($class, $namespace),
            'category'     => $category,
            'template'     => $template,
            'filename'     => $filename,
            'factoryClass' => Util::classTruncated($factoryClass, $namespace),
            'write'        => false, // ne jamais écrire cette config : tout est à copier/coller
        ];

        return $params;
    },
];