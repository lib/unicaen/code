<?php

use UnicaenCode\Util;

return [
    'title'     => 'Création d\'un nouveau trait',
    'class'     => [
        'label' => 'Classe de l\'entité visée',
        'value' => 'Application\Entity\Album',
    ],
    'useGetter' => [
        'type'  => 'checkbox',
        'label' => 'Générer un getter dans le trait',
        'value' => true,
    ],
    'subDir'    => [
        'type'  => 'checkbox',
        'label' => 'Le trait sera placé dans un sous-dossier dédié',
        'value' => false,
    ],
    'generator' => function (array $params): array {
        $class     = $params['class'] ?? null;
        $template  = $params['template'] ?? 'AwareTrait';
        $useGetter = (bool)($params['useGetter'] ?? true);
        $subDir    = (bool)($params['subDir'] ?? true);
        $type      = $params['type'] ?? Util::classType($class);

        if (!$type && false !== strpos($class, 'Entity')) {
            $type = 'Entity';
        }

        $namespace = Util::classNamespace($class) . ($subDir ? '\\Traits' : '');

        $targetClass     = $class;
        $targetClassname = Util::classClassname($class);

        $classname = $targetClassname . 'AwareTrait';

        $module = Util::classModule($class);

        if ('Entity' == $type) {
            $method = Util::classClassname($class);
        }elseif('Fieldset' == $type){
            $method     = Util::classTruncated($class, $module);
            if (0 === strpos($method, 'Form\\')){
                $method = 'Fieldset\\'.substr($method, 5);
            }
            $methodBase = Util::classRoot($method);
            $method     = Util::classCamelCase($method);
            if (substr($method, -strlen($methodBase)) == $methodBase) {
                $method = substr($method, 0, -strlen($methodBase));
            }
        } else {
            $method     = Util::classTruncated($class, $module);
            $methodBase = Util::classRoot($method);
            $method     = Util::classCamelCase($method);
            if (substr($method, -strlen($methodBase)) == $methodBase) {
                $method = substr($method, 0, -strlen($methodBase));
            }
        }
        $variable = lcfirst($method);

        $filename = Util::classFilename($namespace . '\\' . $classname);

        $params['type']            = $type;
        $params['template']        = $template;
        $params['filename']        = $filename;
        $params['namespace']       = $namespace;
        $params['subDir']          = $subDir;
        $params['targetClass']     = $targetClass;
        $params['classname']       = $classname;
        $params['targetClassname'] = $targetClassname;
        $params['variable']        = $variable;
        $params['method']          = $method;
        $params['useGetter']       = $useGetter;

        return $params;
    },
];