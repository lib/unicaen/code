<?php

use UnicaenCode\Util;

$sPrivileges = Util::getContainer()->get('UnicaenPrivilege\Service\Privilege\PrivilegeService');
/* @var $sPrivileges \UnicaenPrivilege\Service\Privilege\PrivilegeService */

$privileges = $sPrivileges->getList();

$privilegesConsts = [];
$constMaxLen      = 0;
foreach ($privileges as $privilege) {
    $value = var_export($privilege->getFullCode(), true);
    $const = strtoupper($privilege->getFullCode());
    $const = str_replace(['-', ' '], ['_', '_'], $const);
    if (strlen($const) > $constMaxLen) $constMaxLen = strlen($const);

    if (!defined(\UnicaenPrivilege\Provider\Privilege\Privileges::class . '::' . $const)) {
        $privilegesConsts[$const] = $value;
    }
}

asort($privilegesConsts);

$data = '';
foreach ($privilegesConsts as $const => $value) {
    if ('' !== $data) $data .= "\n";
    $data .= "    const " . str_pad($const, $constMaxLen) . " = $value;";
}


return [
    'title'      => 'Génération de la classe listant les constantes de privilèges',
    'template'   => 'Privileges',
    'privileges' => $data,
    'filename'   => 'module/Application/src/Provider/Privilege/Privileges.php',
];