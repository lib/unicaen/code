<?php

use UnicaenCode\Util;

return [
    'title' => 'Génération de configuration de contrôleur',
    'class'          => [
        'label' => 'Classe du Controller',
        'value' => 'Application\Controller\ExempleController',
    ],
    'factory' => [
        'label' => 'Classe de la factory correspondante',
        'value' => 'Application\Service\ExempleControllerFactory',
    ],
    'name'         => [
        'label' => 'Nom ou alias pour y accéder',
        'value' => null,
    ],

    'generator' => function (array $params): array {
        $class    = $params['class'] ?? null;
        $template = $params['template'] ?? 'ControllerConfig';
        $author   = $params['author'] ?? Util::getAuthor();
        $route    = $params['route'] ?? null;

        $namespace = Util::classModule($class, Util::CLASS_MODULE_NAMESPACE);
        $classname = Util::classClassname($class);

        if (substr($classname, -strlen('Controller')) == 'Controller') {
            $name = substr($class, 0, -strlen('Controller'));

            if (!$route) {
                $route = lcfirst(substr($classname, 0, -strlen('Controller')));
            }
        } else {
            $name = $class;
            if (!$route) {
                $route = lcfirst($classname);
            }
        }

        $filename = Util::classModule($class, Util::CLASS_MODULE_CONFIG)['relPath'] . '/config/module.config.php';

        $params['factoinvo'] = 'factories';
        $params['namespace'] = $namespace;
        $params['classname'] = $classname;
        $params['template']  = $template;
        $params['name']      = $name;
        $params['author']    = $author;
        $params['route']     = $route;
        $params['filename']  = $filename;
        $params['write']     = false; // ne jamais écrire cette config : tout est à copier/coller

        return $params;
    },
];