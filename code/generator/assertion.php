<?php

use UnicaenCode\Util;

return [
    'title' => 'Création d\'une nouvelle assertion',

    'generator' => function (array $params): array {
        $class    = $params['class'] ?? null;
        $type     = $params['type'] ?? Util::classType($class) ?? 'Assertion';
        $author   = $params['author'] ?? Util::getAuthor();
        $template = $params['template'] ?? 'Assertion';

        $namespace = Util::classNamespace($class);
        $classname = Util::classClassname($class);
        $filename  = Util::classFilename($class);

        $params['namespace'] = $namespace;
        $params['classname'] = $classname;
        $params['author']    = $author;
        $params['template']  = $template;
        $params['type']      = $type;
        $params['filename']  = $filename;
        $params['config']    = true;

        return $params;
    },

    'class' => [
        'label' => 'Nom de classe de l\'assertion',
        'value' => 'Application\Assertion\ExempleAssertion',
    ],

    'factory' => [
        'type'  => 'checkbox',
        'label' => 'Générer une factory',
        'value' => true,
    ],

    'subDir' => [
        'type'  => 'checkbox',
        'label' => 'Les traits, interfaces et Factory seront placés dans des sous-dossiers dédiés',
        'value' => false,
    ],

];