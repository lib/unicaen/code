<?php

use UnicaenCode\Util;

return [
    'title'          => 'Création d\'une nouvelle factory',
    'class'          => [
        'label' => 'Classe de l\'entité visée',
        'value' => 'Application\Service\ExempleService',
    ],
    'type'      => [
        'type'  => 'text',
        'label' => 'Type de classe',
    ],
    'subDir'         => [
        'type'  => 'checkbox',
        'label' => 'La factory sera placée dans un sous-dossier dédié',
        'value' => false,
    ],
    'generator' => function (array $params): array {
        $class    = $params['class'] ?? null;
        $template = $params['template'] ?? 'Factory';
        $subDir   = (bool)($params['subDir'] ?? true);
        $type     = $params['type'] ?? Util::classType($class);
        $author   = $params['author'] ?? Util::getAuthor();

        $namespace       = Util::classNamespace($class) . ($subDir ? '\\Factory' : '');
        $targetClass     = $class;
        $targetClassname = Util::classClassname($targetClass);
        $classname       = $targetClassname . 'Factory';
        $variable        = lcfirst($type ?: lcfirst($targetClass));
        $filename        = Util::classFilename($namespace . '\\' . $classname);

        $params['class']           = $namespace . "\\" . $classname;
        $params['namespace']       = $namespace;
        $params['subDir']          = $subDir;
        $params['targetClass']     = $targetClass;
        $params['classname']       = $classname;
        $params['author']          = $author;
        $params['targetClassname'] = $targetClassname;
        $params['variable']        = $variable;
        $params['filename']        = $filename;
        $params['template']        = $template;

        return $params;
    },
];