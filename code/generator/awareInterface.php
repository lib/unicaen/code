<?php

use UnicaenCode\Util;

return [
    'title'          => 'Création d\'une nouvelle interface',
    'class'          => [
        'label' => 'Classe de l\'entité visée',
        'value' => 'Application\Entity\Album',
    ],
    'useGetter'      => [
        'type'  => 'checkbox',
        'label' => 'Générer un getter dans l\'interface',
        'value' => true,
    ],
    'subDir'         => [
        'type'  => 'checkbox',
        'label' => 'L\'interface sera placée dans un sous-dossier dédié',
        'value' => false,
    ],
    'generator' => function (array $params): array {
        $class     = $params['class'] ?? null;
        $template  = $params['template'] ?? 'AwareInterface';
        $useGetter = (bool)($params['useGetter'] ?? true);
        $subDir    = (bool)($params['subDir'] ?? true);
        $type      = $params['type'] ?? ($class ? Util::classType($class) : null);

        $namespace = Util::classNamespace($class) . ($subDir ? '\\Interfaces' : '');

        $targetClass     = $class;
        $targetClassname = Util::classClassname($class);

        $classname = $targetClassname . 'AwareInterface';

        $module = Util::classModule($class);

        if ('Entity' == $type) {
            $method = Util::classClassname($class);
        } else {
            $method     = Util::classTruncated($class, $module);
            $methodBase = Util::classRoot($method);
            $method     = Util::classCamelCase($method);
            if (substr($method, -strlen($methodBase)) == $methodBase) {
                $method = substr($method, 0, -strlen($methodBase));
            }
        }
        $variable = lcfirst($method);

        $filename = Util::classFilename($namespace . '\\' . $classname);

        $params['template']        = $template;
        $params['filename']        = $filename;
        $params['namespace']       = $namespace;
        $params['subDir']          = $subDir;
        $params['targetClass']     = $targetClass;
        $params['classname']       = $classname;
        $params['targetClassname'] = $targetClassname;
        $params['variable']        = $variable;
        $params['method']          = $method;
        $params['useGetter']       = $useGetter;

        return $params;
    },
];