<?php

use UnicaenCode\Util;


return [
    'title'     => 'Génération du PhpRenderer de l\'application',
    'class'     => 'Application\View\Renderer\PhpRenderer',
    'generator' => function (array $params) {
        Util::getContainer()->setService('request', new \Laminas\Http\Request());

        $vhs = Util::introspection()->getViewHelpers();

        $methods = [];
        foreach ($vhs as $key => $vhClass) {
            if ($vhClass) {
                $rc = new \ReflectionClass($vhClass);
                if ($rc->hasMethod('__invoke')) {
                    $method    = $rc->getMethod('__invoke');
                    $methods[] = ' * ' . Util::getMethodDocDeclaration($rc->getMethod('__invoke'), $key, '\\' . $vhClass);
                } else {
                    $methods[] = " * @method \\$vhClass $key()";
                }
            }
        }

        $params['template'] = 'ApplicationPhpRenderer';
        $params['filename'] = Util::classFilename($params['class']);
        $params['methods']  = implode("\n", $methods);

        return $params;
    },
];
