<?php

use UnicaenCode\Util;

return [
    'title'     => 'Création d\'une nouvelle commande console',
    'generator' => function (array $params): array {
        $class    = $params['class'] ?? null;
        $type     = $params['type'] ?? Util::classType($class) ?? 'Command';
        $author   = $params['author'] ?? Util::getAuthor();
        $template = $params['template'] ?? 'Command';

        $namespace = Util::classNamespace($class);
        $classname = Util::classClassname($class);
        $filename  = Util::classFilename($class);

        $params['namespace'] = $namespace;
        $params['classname'] = $classname;
        $params['author']    = $author;
        $params['template']  = $template;
        $params['type']      = $type;
        $params['filename']  = $filename;
        $params['commandConfig'] = true;

        return $params;
    },

    'class'          => [
        'label' => 'Classe de la commande',
        'value' => 'Application\Command\ExempleCommand',
    ],
    'name'    => [
        'label' => 'Nom pour l\'accès à la commande',
        'value' => 'module:exemple',
    ],
    'factory'        => [
        'type'  => 'checkbox',
        'label' => 'Générer une factory',
        'value' => true,
    ],
    'subDir'         => [
        'type'  => 'checkbox',
        'label' => 'La Factory sera placée dans un sous-dossier dédié',
        'value' => false,
    ],
];