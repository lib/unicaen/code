<?php

use UnicaenCode\Util;

return [
    'title'     => 'Création d\'un nouveau service',
    'generator' => function (array $params): array {
        $class    = $params['class'] ?? null;
        $type     = $params['type'] ?? Util::classType($class) ?? 'Service';
        $author   = $params['author'] ?? Util::getAuthor();
        $template = $params['template'] ?? 'Service';

        $namespace = Util::classNamespace($class);
        $classname = Util::classClassname($class);
        $filename  = Util::classFilename($class);

        $params['namespace'] = $namespace;
        $params['classname'] = $classname;
        $params['author']    = $author;
        $params['template']  = $template;
        $params['type']      = $type;
        $params['filename']  = $filename;
        $params['config']    = true;

        return $params;
    },

    'class'          => [
        'label' => 'Classe du service',
        'value' => 'Application\Service\ExempleService',
    ],
    'awareTrait'     => [
        'type'  => 'checkbox',
        'label' => 'Générer un trait',
        'value' => true,
    ],
    'awareInterface' => [
        'type'  => 'checkbox',
        'label' => 'Générer une interface',
        'value' => false,
    ],
    'useGetter'      => [
        'type'  => 'checkbox',
        'label' => 'Générer des getters dans les traits et les interfaces',
        'value' => true,
    ],
    'factory'        => [
        'type'  => 'checkbox',
        'label' => 'Générer une factory',
        'value' => true,
    ],
    'subDir'         => [
        'type'  => 'checkbox',
        'label' => 'Les traits, interfaces et Factory seront placés dans des sous-dossiers dédiés',
        'value' => false,
    ],
];