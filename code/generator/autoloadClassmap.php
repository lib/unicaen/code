<?php

use UnicaenCode\Util;

return [
    'title'  => 'Génération d\'une Classmap pour optimiser l\'autoload',
    'module' => [
        'label' => 'Module concerné',
        'type'  => 'selectModule',
    ],

    'generator' => function (array $params) {
        $moduleName = $params['module'];
        $module     = Util::introspection()->getModule($moduleName);
        $map        = Util::introspection()->getClasses(namespace: $module['namespace']);

        $mc = 0;
        foreach ($map as $c) {
            $lc = strlen($c);
            if ($lc > $mc) $mc = $lc;
        }
        $php = '';
        foreach ($map as $c) {
            $rc = new ReflectionClass($c);
            $f = substr($rc->getFileName(), strlen($module['absPath']));

            $php .= "    " . str_pad("'$c'", $mc + 2, ' ') . " => __DIR__ . '$f',\n";
        }

        $params['template'] = 'autoload_classmap';
        $params['filename'] = $module['relPath'] . '/autoload_classmap.php';
        $params['map']      = trim($php);

        return $params;
    },
];