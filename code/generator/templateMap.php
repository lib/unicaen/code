<?php

use UnicaenCode\Util;

return [
    'title'    => 'Génération d\'une TemplateMap pour optimiser le traitement des vues',
    'template' => 'template_map',
    'module' => [
        'label' => 'Module concerné',
        'type'  => 'selectModule',
    ],

    'generator' => function (array $params) {
        $moduleName = $params['module'];
        $module     = Util::introspection()->getModule($moduleName);
        $map        = Util::introspection()->getViews($moduleName);

        $mc = 0;
        foreach ($map as $f) {
            $lc = strlen($f);
            if ($lc > $mc) $mc = $lc;
        }
        $php = '';
        foreach ($map as $f) {
            $c   = $f;
            $php .= "    " . str_pad("'$c'", $mc + 2, ' ') . " => __DIR__ . '/" . $module['viewDir'] . "/$f.phtml',\n";
        }

        $params['filename'] = $module['relPath'] . '/template_map.php';
        $params['map'] = trim($php);

        return $params;
    },
];