<?php

use UnicaenCode\Util;

return [
    'title'          => 'Création d\'un nouveau trait',
    'class'          => [
        'label' => 'Classe de l\'entité visée',
        'value' => 'Application\Entity\Album',
    ],
    'type'           => [
        'type'    => 'select',
        'label'   => 'Type de formulaire (Form ou Fieldset)',
        'options' => ['Form' => 'Form', 'Fieldset' => 'Fieldset', 'Service' => 'Service'],
        'value'   => 'Service',
    ],
    'factory' => [
        'label' => 'Classe de la factory correspondante',
        'value' => 'Application\Service\ExempleServiceFactory',
    ],
    'name'         => [
        'label' => 'Nom ou alias pour y accéder',
        'value' => null,
    ],
    'generator' => function (array $params): array {
        $class        = $params['class'];
        $factoryClass = $params['factory']['class'] ?? $class;
        $type         = $params['type'] ?? 'Service';
        $namespace    = Util::classModule($class, Util::CLASS_MODULE_CONFIG)['namespace'];
        $name         = $params['name'] ?? $class;
        $factoinvo    = ((bool)($params['factory']['class'] ?? false)) ? 'factories' : 'invokables';
        $filename     = Util::classModule($class, Util::CLASS_MODULE_CONFIG)['relPath'] . '/config/module.config.php';

        switch ($type) {
            case 'Form':
            case 'Fieldset':
                $category = 'form_elements';
            break;
            case 'Controller':
                $category = 'controllers';
            break;
            case 'ViewHelper':
                $category = 'view_helpers';
            break;
            default:
                $category = 'service_manager';
        }

        $params = [
            'template'     => ($category == 'view_helpers' ? 'ViewHelper' : '') . 'Config',
            'filename'     => $filename,
            'namespace'    => $namespace,
            'category'     => $category,
            'factoinvo'    => $factoinvo,
            'name'         => $name,
            'class'        => Util::classTruncated($class, $namespace),
            'factoryClass' => Util::classTruncated($factoryClass, $namespace),
            'write'        => false,
        ];

        return $params;
    },
];