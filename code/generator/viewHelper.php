<?php

use UnicaenCode\Util;

return [
    'title'     => 'Création d\'une nouvelle aide de vue',
    'generator' => function (array $params): array {
        $type     = 'ViewHelper';
        $class    = $params['class'] ?? null;
        $author   = $params['author'] ?? Util::getAuthor();
        $template = $params['template'] ?? 'ViewHelper';


        $namespace = Util::classNamespace($class);
        $classname = Util::classClassname($class);
        $filename  = Util::classFilename($class);

        $params['namespace'] = $namespace;
        $params['classname'] = $classname;
        $params['author']    = $author;
        $params['template']  = $template;
        $params['type']      = $type;
        $params['filename']  = $filename;
        $params['config']    = true;

        return $params;
    },

    'class'   => [
        'label' => 'Nom de classe de l\'aide de vue',
        'value' => 'Application\View\Helper\ExempleViewHelper',
    ],
    'name'    => [
        'label' => 'Nom pour le ServiceLocator (en lowerCamelCase)',
        'value' => 'exemple',
    ],
    'factory' => [
        'type'  => 'checkbox',
        'label' => 'Générer une factory',
        'value' => true,
    ],
    'subDir'  => [
        'type'  => 'checkbox',
        'label' => 'Les traits, interfaces et Factory seront placés dans des sous-dossiers dédiés',
        'value' => false,
    ],
];