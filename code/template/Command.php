<?php

namespace <namespace>;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Description of <classname>
 *
 * @author <author>
 */
class <classname> extends Command
{
    protected function configure(): void
    {
        $this->setDescription('... description à adapter ...');
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io  = new SymfonyStyle($input, $output);

        return Command::SUCCESS;
    }
}