<?php

namespace <namespace>;

use Laminas\View\Helper\AbstractHtmlElement;



/**
 * Description of <classname>
 *
 * @author <author>
 */
class <classname> extends AbstractHtmlElement
{

    /**
     *
     * @return self
     */
    public function __invoke()
    {
        return $this;
    }



    /**
     * Retourne le code HTML.
     *
     * @return string Code HTML
     */
    public function __toString(): string
    {
        return $this->render();
    }



    /**
     *
     *
     * @return string Code HTML
     */
    public function render(): string
    {
        $r = '';
        /* Complétez */

        return $r;
    }

}