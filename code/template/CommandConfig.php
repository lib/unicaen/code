<?php

namespace <namespace>;

return [
    'laminas-cli' => [
        'commands' => [
            '<name>' => <class>::class,
        ],
    ],

    '<category>'     => [
        '<factoinvo>' => [
            <class>::class => <factoryClass>::class,
        ],
    ],
];