<?php

namespace <namespace>;

return [
    '<category>'     => [
        '<factoinvo>' => [
            <class>::class => <factoryClass>::class,
        ],
    ],
];