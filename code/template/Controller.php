<?php

namespace <namespace>;

use Laminas\Mvc\Controller\AbstractActionController;


/**
 * Description of <classname>
 *
 * @author <author>
 */
class <classname> extends AbstractActionController
{

    public function indexAction()
    {
        return [];
    }

}