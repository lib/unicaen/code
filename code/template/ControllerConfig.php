<?php

namespace <namespace>;

use UnicaenPrivilege\Guard\PrivilegeController;

return [

    /* Routes (à personnaliser) */
    'router'          => [
        'routes' => [
            '<route>' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/<route>',
                    'defaults' => [
                        'controller' => '<name>',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    /* Placez ici vos routes filles */
                ],
            ],
        ],
    ],

    /* Exemple de menu (à personnaliser) */
    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    '<route>' => [
                        'label'    => '<route>',
                        'route'    => '<route>',
                        'resource' => PrivilegeController::getResourceId('<name>','index'),
                    ],
                ],
            ],
        ],
    ],

    /* Droits d'accès */
    'bjyauthorize'    => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => '<name>',
                    'action'     => ['index'],
                    'privileges' => [
                        /* Placez ici les privilèges concernés */
                    ],
                ],
            ],
        ],
    ],

    /* Déclaration du contrôleur */
    'controllers'     => [
        '<factoinvo>' => [
            '<name>' => <factoryClass>::class,
        ],
    ],
];