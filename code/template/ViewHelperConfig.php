<?php

namespace <namespace>;

return [
    '<category>'     => [
        '<factoinvo>' => [
            '<name>' => <factoryClass>::class,
        ],
    ],
];