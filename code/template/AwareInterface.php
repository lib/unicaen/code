<?php

namespace <namespace>;

<if subDir>use <targetClass>;
<endif subDir>

/**
 * Description of <classname>
 *
 * @author UnicaenCode
 */
interface <classname>
{
    /**
     * @param <targetClassname>|null $<variable>
     *                      
     * @return self
     */
    public function set<method>( ?<targetClassname> $<variable> );
<if useGetter notrim>


    public function get<method>(): ?<targetClassname>;
<endif useGetter>
}