<?php

/**
 * @var $this       \Application\View\Renderer\PhpRenderer
 * @var $container  \Psr\Container\ContainerInterface
 */

?>
    <h1>Traits déclarés et non utilisés</h1>

    <div class="alert alert-warning">Attention : dans certains cas, le trait n'est pas utilisé dans le fichier courant, mais utilisé de manière déportée. Dans
        ce cas, il ne faut pas supprimer les "use".
    </div>
    <div class="alert alert-warning">Attention aussi : les "use" avec des "as" sont mal gérés : fichiers à purger avec parcimonie, donc!
    </div>

<?php
$introspection = \UnicaenCode\Util::introspection();


$traits = array_flip($introspection->getTraits());
foreach ($traits as $trait => $null) {
    $traitParams    = $introspection->getTraitParams($trait);
    $traits[$trait] = $traitParams;
    if (!$traitParams['aware']) {
        unset($traits[$trait]);
    }
    if (str_contains($traitParams['class'], '\Entity')) {
        unset($traits[$trait]);
    }
}


$modules = $introspection->getModules();
foreach ($modules as $module) {
    $files = $introspection->getSrcs($module['name'], true);
    echo '<h2>' . $module['name'] . '</h2>';

    echo '<table class="table table-condensed table-bordered table-hover">';
    echo '<tr><th>Fichier</th><th>Trait</th><td>Non déclaré</td><td>Non utilisé</td></tr>';
    foreach ($files as $file) {
        $c    = file_get_contents($file);
        $file = substr($file, strlen($module['absPath']) + 2 + strlen($module['srcDir']));
        foreach ($traits as $trait) {
            if (str_contains($c, $trait['class'])) {

                $class = 'use ' . \UnicaenCode\Util::classClassname($trait['class']) . ';';

                $notDeclared = !str_contains($c, $class);

                $notUsed = true;
                if ($trait['getter'] && str_contains($c, '$this->' . $trait['getter'] . '(')) {
                    $notUsed = false;
                }
                if ($trait['setter'] && str_contains($c, '$this->' . $trait['setter'] . '(')) {
                    $notUsed = false;
                }


                if ($notDeclared || $notUsed) {
                    echo '<tr><td>' . $file . '</td><td>' . $trait['class'] . '</td><td>' . ($notDeclared ? 'X' : '') . '</td><td>' . ($notUsed ? 'X' : '') . '</td></tr>';
                }
            }
        }
    }
    echo '</table>';
}