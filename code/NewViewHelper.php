<?php

/**
 * @var $this       \Application\View\Renderer\PhpRenderer
 * @var $container  \Psr\Container\ContainerInterface
 */

\UnicaenCode\Util::codeGenerator()->generer('viewHelper');

if (!\UnicaenCode\Util::inConsole()) {
    ?>

    <h3>Etape 4 : Mise à jour de votre phpRenderer</h3>

    <a href="<?php echo $this->url('unicaen-code', ['view' => 'GenerateApplicationPhpRenderer']); ?>">Aller au générateur de phpRenderer</a>
    <?php
}