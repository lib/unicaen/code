<?php

/**
 * @var $this       \Application\View\Renderer\PhpRenderer
 * @var $container  \Psr\Container\ContainerInterface
 */

$params = \UnicaenCode\Util::codeGenerator()->getGenerator('applicationPhpRenderer');

if (!\UnicaenCode\Util::inConsole()){
    ?>
    <div class="alert alert-warning">
        <span class="glyphicon glyphicon glyphicon-thumbs-up"
              style="float: left;font-size:50pt;margin-right:.2em;margin-bottom:.2em"></span>
        Le PhpRenderer permet de bénéficier de l'auto-complétion des view helpers depuis vos vues.
        Pour l'utiliser, il convient de déclarer la ligne suivante au début de chaque vue :

        <?php phpDump('/* @var $this \\' . $params['class'] . ' */'); ?>

    </div>
    <?php
}

\UnicaenCode\Util::codeGenerator()->generer($params);