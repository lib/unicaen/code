<?php

namespace UnicaenCode;

$unicaenCodeDir = dirname(__DIR__);

return [
    'unicaen-code' => [
        'sqlformatter-file'    => $unicaenCodeDir . '/vendor/sql-formatter-master/lib/SqlFormatter.php',
        'geshi-file'           => $unicaenCodeDir . '/vendor/geshi/geshi.php',
        'view-dirs'            => [$unicaenCodeDir . '/code'],
        'template-dirs'        => [$unicaenCodeDir . '/code/template'],
        'generator-dirs'       => [$unicaenCodeDir . '/code/generator'],
        'generator-output-dir' => '/tmp/UnicaenCode',
        'generator-cmd'        => 'php public/index.php UnicaenCode',
    ],

    'doctrine' => [
        'sql_logger_collector' => [
            'orm_default' => [
                'sql_logger' => DBAL\Logging\DebugStack::class,
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            'UnicaenCode\Controller' => Controller\Factory\ControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories'  => [
            Service\ConfigService::class        => Service\Factory\ConfigServiceFactory::class,
            Service\CollectorService::class     => Service\Factory\CollectorServiceFactory::class,
            Service\IntrospectionService::class => Service\Factory\IntrospectionServiceFactory::class,
            Service\CodeGeneratorService::class => Service\Factory\CodeGeneratorServiceFactory::class,
            Command\RunCommand::class           => Command\RunCommandFactory::class,
        ],
        'invokables' => [
            DBAL\Logging\DebugStack::class => DBAL\Logging\DebugStack::class,
        ],
    ],

    'view_manager' => [
        'template_map' => [
            'unicaen-code/index' => $unicaenCodeDir . '/view/unicaen-code/index.phtml',
            'unicaen-code/ajax'  => $unicaenCodeDir . '/view/unicaen-code/ajax.phtml',
            'unicaen-code/menu'  => $unicaenCodeDir . '/view/unicaen-code/menu.phtml',
            'laminas-developer-tools/toolbar/unicaen-code'
                                 => $unicaenCodeDir . '/view/laminas-developer-tools/toolbar/unicaen-code.phtml',
            'laminas-developer-tools/toolbar/doctrine-orm-queries'
                                 => __DIR__ . '/../view/laminas-developer-tools/toolbar/doctrine-orm-queries.phtml',
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-code' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/unicaen-code[/:view]',
                    'defaults' => [
                        'controller' => 'UnicaenCode\Controller',
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],

    'laminas-cli'             => [
        'commands' => [
            'unicaen-code:run' => Command\RunCommand::class,
        ],
    ],

    // intégration à la Laminas Developer Toolbar
    'laminas-developer-tools' => [
        'profiler' => [
            'collectors' => [
                Service\CollectorService::class => Service\CollectorService::class,
            ],
        ],
        'toolbar'  => [
            'entries' => [
                Service\CollectorService::class => 'laminas-developer-tools/toolbar/unicaen-code',
            ],
        ],
    ],

    // Intégration à BjyAuthorize
    'bjyauthorize'            => [
        'guards' => [
            'BjyAuthorize\Guard\Controller' => [
                ['controller' => 'UnicaenCode\Controller', 'roles' => []],
            ],
        ],
    ],
];