<?php

namespace UnicaenCode\DBAL\Logging;

/**
 * Ajout par rapport à la classe mère :
 * collecte de l'endroit dans le code de l'appli où a été lancée la requête.
 */
class DebugStack extends \Doctrine\DBAL\Logging\DebugStack
{
    public function startQuery($sql, array $params = null, array $types = null)
    {
        parent::startQuery($sql, $params, $types);

        if ($this->enabled) {
            $trace = (new \Exception())->getTraceAsString();
            $this->queries[$this->currentQuery]['trace'] = $this->extractLastCallFromTrace($trace);
        }
    }

    protected function extractLastCallFromTrace($traceAsString)
    {
        $trace = explode(PHP_EOL, $traceAsString);

        foreach (array_reverse($trace) as $line) {
            // on ne s'intéresse qu'aux fichiers situés dans le répertoire module
            $fileIsInApp = ($pos = strpos($line, $text = '/module/')) !== false;
            if ($fileIsInApp) {
                return substr($line, $pos + strlen($text));
            }
        }

        return null;
    }
}