<?php

namespace UnicaenCode\Controller\Factory;

use Psr\Container\ContainerInterface;
use UnicaenCode\Controller\Controller;
use UnicaenCode\Service\ConfigService;

class ControllerFactory
{

    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $controller = new Controller();
        $controller->setServiceConfig($container->get(ConfigService::class));

        return $controller;
    }

}