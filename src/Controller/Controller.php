<?php

namespace UnicaenCode\Controller;

use UnicaenCode\Service\Traits\ConfigServiceAwareTrait;
use UnicaenCode\Util;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class Controller extends AbstractActionController
{
    use ConfigServiceAwareTrait;

    public function indexAction()
    {
        $viewName = $this->params()->fromRoute('view');
        if ($viewName) {
            return $this->codeAction($viewName);
        }

        /* @var $collector \UnicaenCode\Collector */
        $collector = Util::collector();
        $items      = $collector->getItems();

        $viewModel = new ViewModel();
        $viewModel->setTemplate('unicaen-code/menu');
        $viewModel->setVariables(['items' =>$items]);

        return $viewModel;
    }



    protected function codeAction(string $viewName)
    {
        Util::$currentView = $viewName;
        Util::$inputParams = $_REQUEST;

        $viewModel = new ViewModel;
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTemplate('unicaen-code/ajax');
        } else {
            $viewModel->setTemplate('unicaen-code/index');
        }

        $viewName = str_replace('.', '/', $viewName);

        $viewModel->setVariables([
                                     'container' => Util::getContainer(),
                                     'viewName'  => $viewName,
                                     'viewFile'  => $this->getViewFile($viewName),
                                 ]);

        return $viewModel;
    }


    private function getViewFile($viewName)
    {
        $viewDirs = $this->getServiceConfig()->getViewDirs();
        $viewFile = null;
        foreach ($viewDirs as $viewDir) {
            if (file_exists($viewDir . '/' . $viewName . '.php')) {
                $viewFile = $viewDir . '/' . $viewName . '.php';
                break;
            }
        }

        return $viewFile;
    }
}