<?php

namespace UnicaenCode\Manager;

use Psr\Container\ContainerInterface;
use Laminas\View\HelperPluginManager;

class ViewHelperManager extends HelperPluginManager
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct($configOrContainerInstance = null, array $v3config = [])
    {
        $this->container = $configOrContainerInstance;

        parent::__construct($configOrContainerInstance, $v3config);
    }



    public function getClasses(): array
    {
        $names = array_keys($this->aliases);

        $conf = $this->getConfig();

        if (isset($conf['aliases'])){
            $names = array_merge($names, array_keys($conf['aliases']));
        }
        if (isset($conf['invokables'])){
            $names = array_merge($names, array_keys($conf['invokables']));
        }
        if (isset($conf['factories'])){
            $names = array_merge($names, array_keys($conf['factories']));
        }

        $names = array_unique($names);
        foreach( $names as $i => $name ){
            if (false !== strpos($name, "\\")){
                unset($names[$i]);
            }

            if (false !== strpos($name, "/")){
                unset($names[$i]);
            }

            if (0 === $name){
                unset($names[$i]);
                continue;
            }

            /* Si il a une majuscule au début alors qu'il y en a sans majuscule au début */
            $firstChar = $name[0];
            if ($firstChar == strtoupper($firstChar)){
                if (array_search(lcfirst($name), $names)) {
                    unset($names[$i]);
                }
            }

            /* On supprime les helpers avec des _ alors qu'il en existe déjà identiques qui n'en ont pas */
            if (false !== strpos($name,'_')) {
                foreach( $names as $name2 ){
                    if (strtolower($name2) == strtolower(str_replace('_','',$name))){
                        unset($names[$i]);
                    }
                }
            }

            /* On supprime les helpers en minuscule s'il existe déjà en lowerCamelCase */
            if ($name == strtolower($name)){
                foreach( $names as $name3 ){
                    if ($name3 != $name && strtolower($name3) == $name){
                        unset($names[$i]);
                    }
                }
            }

            /* On supprime formTextArea, car doublon un peu crado... */
            if ($name == 'formTextArea'){
                unset($names[$i]);
            }
        }

        $result = [];
        $container = $this->container->get('ViewHelperManager');
        foreach( $names as $name ) {
            if ($container->has($name)) {
                try {
                    $object = @$container->get($name);
                    $result[$name] = is_object($object) ? get_class($object) : gettype($object);
                }catch(\Exception|\Error $e){
                    $result[$name] = null;
                }
            }
        }
        asort($result);

        return $result;
    }



    public function getConfig()
    {
        $conf = $this->container->get('Config');
        if (isset($conf['view_helpers'])){
            return $conf['view_helpers'];
        }else{
            return [];
        }
    }

}