<?php

namespace UnicaenCode\Manager;

use Psr\Container\ContainerInterface;


class ServiceManager extends \Laminas\ServiceManager\ServiceManager
{
    /**
     * @var ContainerInterface
     */
    private $container;



    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }



    public function getClasses()
    {
        $names = array_keys($this->aliases);

        $conf = $this->getConfig();

        if (isset($conf['invokables'])) {
            $names = array_merge($names, array_keys($conf['invokables']));
        }
        if (isset($conf['factories'])) {
            $names = array_merge($names, array_keys($conf['factories']));
        }

        $names = array_unique($names);
        $names = array_values($names);

        $result = [];

        $forced = [
            // Exceptions à gérer manuellement, car le code d'origine plante dans certains cas...
            'ConsoleViewManager'                                  => 'Laminas\Mvc\Console\View\ViewManager',
            'ConsoleAdapter'                                      => 'stdClass',
            'BjyAuthorize\Service\RoleDbTableGateway'             => 'Laminas\Db\TableGateway\TableGateway',
            'BjyAuthorize\Provider\Role\ObjectRepositoryProvider' => 'BjyAuthorize\Provider\Role\ObjectRepositoryProvider',
            'BjyAuthorize\Provider\Identity\ZfcUserLaminasDb'     => 'BjyAuthorize\Provider\Identity\ZfcUserLaminasDb',
            'BjyAuthorize\Provider\Rule\Config'                   => 'BjyAuthorize\Provider\Rule\Config',
            'BjyAuthorize\Guard\Route'                            => 'BjyAuthorize\Guard\Route',
            'BjyAuthorize\Provider\Role\Config'                   => 'BjyAuthorize\Provider\Role\Config',
            'UnicaenUtilisateur\Provider\Role\Username'           => 'UnicaenUtilisateur\Provider\Role\Username',
        ];

        foreach ($names as $name) {
            if (isset($forced[$name])) {
                $result[$name] = $forced[$name];
            } else {
                if ($this->container->has($name)) {
                    $object        = @$this->container->get($name);
                    $result[$name] = is_object($object) ? get_class($object) : gettype($object);
                }
            }
        }

        asort($result);

        return $result;
    }



    public function getConfig()
    {
        $conf = $this->container->get('Config');
        if (isset($conf['service_manager'])) {
            return $conf['service_manager'];
        } else {
            return [];
        }
    }

}