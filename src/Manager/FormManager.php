<?php

namespace UnicaenCode\Manager;

use Psr\Container\ContainerInterface;
use Laminas\Form\FormElementManager;
use Laminas\View\HelperPluginManager;

class FormManager extends FormElementManager
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct($configOrContainerInstance = null, array $v3config = [])
    {
        $this->container = $configOrContainerInstance;

        parent::__construct($configOrContainerInstance, $v3config);
    }



    public function getClasses(): array
    {
        $names = array_keys($this->aliases);

        $conf = $this->getConfig();

        if (isset($conf['aliases'])){
            $names = array_merge($names, array_keys($conf['aliases']));
        }
        if (isset($conf['invokables'])){
            $names = array_merge($names, array_keys($conf['invokables']));
        }
        if (isset($conf['factories'])){
            $names = array_merge($names, array_keys($conf['factories']));
        }
        $names = array_unique($names);

        $result = [];
        $container = $this->container->get('FormElementManager');
        foreach( $names as $name ) {
            if ($container->has($name)) {
                $object = @$container->get($name);
                $result[$name] = is_object($object) ? get_class($object) : gettype($object);
            }
        }
        asort($result);

        return $result;
    }



    public function getConfig()
    {
        $conf = $this->container->get('Config');
        if (isset($conf['form_elements'])){
            return $conf['form_elements'];
        }else{
            return [];
        }
    }

}