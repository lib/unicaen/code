<?php

namespace UnicaenCode;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Psr\Container\ContainerInterface;
use UnicaenCode\Service\CodeGeneratorService;
use UnicaenCode\Service\CollectorService;
use UnicaenCode\Service\Config;
use UnicaenCode\Service\ConfigService;
use UnicaenCode\Service\IntrospectionService;
use Laminas\Form\Form;
use Laminas\Mvc\Controller\AbstractActionController;


/**
 *
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class Util
{
    const CLASS_MODULE_NAME      = 1;
    const CLASS_MODULE_NAMESPACE = 2;
    const CLASS_MODULE_CONFIG    = 3;

    private static ContainerInterface $container;

    public static string $currentView;

    public static array $inputParams;



    public static function getContainer(): ContainerInterface
    {
        if (!self::$container) {
            throw new \Exception('Container introuvable ou module UnicaenCode pas encore chargé');
        }

        return self::$container;
    }



    public static function setContainer(ContainerInterface $container)
    {
        self::$container = $container;
    }



    public static function codeGenerator(): CodeGeneratorService
    {
        return self::getContainer()->get(CodeGeneratorService::class);
    }



    public static function collector(): CollectorService
    {
        return self::getContainer()->get(CollectorService::class);
    }



    public static function config(): ConfigService
    {
        return self::getContainer()->get(ConfigService::class);
    }



    public static function introspection(): IntrospectionService
    {
        return self::getContainer()->get(IntrospectionService::class);
    }



    public static function sqlLog($display = true)
    {
        if (self::getEntityManager()) {
            $sqlLogger          = new EchoSQLLogger();
            $sqlLogger->display = $display;
            self::getEntityManager()->getConfiguration()->setSQLLogger($sqlLogger);
        }
    }



    public static function sqlCount($display = false)
    {
        var_dump('Nombre de requêtes SQL exécutées : ' . count(EchoSQLLogger::$queries));
        if ($display) {
            foreach (EchoSQLLogger::$queries as $query) {
                Util::highlight($query['sql'], 'sql', true, ['parameters' => $query['params']]);
            }
        }
    }



    /**
     * @param string|QueryBuilder $sql
     */
    public static function sqlDump($sql)
    {
        self::highlight($sql, 'sql');
    }



    public static function sqlResDump($res)
    {
        if (!is_array($res) || !isset($res[0])) {
            var_dump($res);
        } else {
            ?>
            <table class="table table-bordered table-condensed table-extra-condensed" style="font-size:8pt">
                <tr>
                    <?php foreach ($res[0] as $c => $v): ?>
                        <th><?= $c ?></th>
                    <?php endforeach; ?>
                </tr>
                <?php foreach ($res as $resi): ?>
                    <tr>
                        <?php foreach ($resi as $c => $v): ?>
                            <td><?= $v ?></td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </table>
            <?php
        }
    }



    public static function arrayDump(array $array, int $sub=0)
    {
        if (self::inConsole()){
            $largestK = 0;
            foreach($array as $k => $v){
                if (strlen($k) > $largestK){
                    $largestK = strlen($k);
                }
            }
            foreach($array as $k => $v){
                echo str_pad('', $sub, ' ').str_pad($k, $largestK, ' ').' => ';
                if (is_array($v)){
                    echo "\n";
                    self::arrayDump($v, $sub + 4);
                }else{
                    var_export($v);
                }
                echo "\n";
            }
        }else{
            ?>
            <table class="table table-condensed" style="font-size:8pt;width:auto">
                <?php foreach ($array as $k => $v): ?>
                    <tr>
                        <td style="padding-bottom: 1px;padding-top: 1px;"><?= $k ?></td>
                        <td style="padding-bottom: 1px;padding-top: 1px;"><?php if (is_array($v)) self::arrayDump($v); else var_export($v) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <?php
        }
    }



    public static function isPost(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }



    public static function inConsole()
    {
        return PHP_SAPI == 'cli';
    }



    public static function phpDump($php)
    {
        self::highlight($php, 'php');
    }



    public static function javascriptDump($javascript)
    {
        self::highlight($javascript, 'jquery');
    }



    public static function cssDump($css)
    {
        self::highlight($css, 'css');
    }



    public static function htmlDump($html)
    {
        self::highlight($html, 'html5');
    }



    public static function xmlDump($xml)
    {
        self::highlight($xml, 'xml');
    }



    public static function varDump($variable)
    {
        var_dump(self::prepareDump($variable));
    }



    public static function prepareDump($variable)
    {
        if (is_array($variable)) {
            foreach ($variable as $k => $v) {
                $variable[$k] = self::prepareDump($v);
            }
        } elseif ($variable instanceof \Doctrine\ORM\PersistentCollection) {
            $result = [];
            foreach ($variable as $k => $v) {
                $result[$k] = self::prepareDump($v);
            }
            $variable = $result;
        } elseif (is_object($variable)) {
            if (method_exists($variable, 'getId')) {
                $variable = 'OBJET : ' . get_class($variable) . ';ID=' . $variable->getId() . ';' . $variable;
            } else {
                $variable = 'OBJET : ' . get_class($variable) . ';' . $variable;
            }
        }

        return $variable;
    }



    private static function normalizeBacktraceLine($data)
    {
        $line = [];

        $class    = isset($data['class']) ? $data['class'] : null;
        $type     = isset($data['type']) ? $data['type'] : null;
        $function = isset($data['function']) ? $data['function'] : null;
        $file     = isset($data['file']) ? $data['file'] : null;
        $line     = isset($data['line']) ? $data['line'] : null;
        $oargs    = isset($data['args']) ? $data['args'] : null;

        if (0 === strpos($file, getcwd() . '/')) {
            $file = str_replace(getcwd() . '/', '', $file);
        }

        if (is_array($oargs)) {
            $args = [];
            foreach ($oargs as $index => $arg) {
                if (is_object($arg)) {
                    $arg = get_class($arg);
                } elseif (is_array($arg)) {
                    $arg = 'Array';
                } else {
                    $arg = var_export($arg, true);
                }
                $args[] = $arg;
            }
            $args = ' (' . implode(', ', $args) . ')';
        } else {
            $args = $oargs;
        }


        $inVendor = 0 === strpos($file, 'vendor');

        return compact('class', 'type', 'function', 'args', 'file', 'line', 'inVendor');
    }



    /**
     * Affiche sur la sortie HTML une trace de débogage
     */
    public static function dumpBacktrace($showInVendor = true, $onlyFirst = false)
    {
        $bt = debug_backtrace(0);
        foreach ($bt as $index => $bl) {
            if (isset($bl['function']) && isset($bl['class']) && $bl['class'] == __CLASS__ && $bl['function'] == 'dumpBacktrace') {
                unset($bt[$index]);
            } else {
                $bl         = self::normalizeBacktraceLine($bl);
                $bt[$index] = $bl;
            }
        }

        $firstDone = false;
        foreach ($bt as $index => $bl) {
            if (empty($bl['file']) && isset($bt[$index + 1])) {
                $nbl            = $bt[$index + 1];
                $bl['inVendor'] = $nbl['inVendor'];
            }
            if ($showInVendor || !$bl['inVendor']) {
                $bt[$index] = $bl;
                $firstDone  = true;
            } else {
                unset($bt[$index]);
            }
        }

        if ($onlyFirst && count($bt) > 0) {
            $bt = [reset($bt)];
        }

        $index = 0;
        ?>

        <style>
            .backtrace {
                display: block;
                padding: 9.5px;
                margin: 0px 0px 10px;
                font-size: 13px;
                line-height: 1.42857;
                color: #333;
                word-break: break-all;
                word-wrap: break-word;
                background-color: #F5F5F5;
                border: 1px solid #CCC;
                border-radius: 4px;
                font-family: Menlo, Monaco, Consolas, "Courier New", monospace;
            }

            #navbar .backtrace {
                display: none;
            }

            .backtrace pre {
                padding: 3px;
                font-size: 8pt;
                background-color: white
            }
        </style>
        <div class="backtrace"><?php foreach ($bt as $bl): ?>
                <div style="white-space:nowrap<?php if ($bl['inVendor']) echo ';opacity:.5' ?>">
                    <?php
                    echo $index++ . ' ' . $bl['file'];
                    if ($bl['line']) {
                        echo ' <span class="badge">' . $bl['line'] . '</span>';
                    }
                    ?>
                </div>
                <div style="margin-left:8em;margin-top:1px;margin-bottom:5px<?php if ($bl['inVendor']) echo ';opacity:.5' ?>">
                    <?php
                    self::highlight($bl['class'] . $bl['type'] . $bl['function'] . $bl['args'], 'php');
                    ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php
    }



    public static function highlight($data, $language = 'php', $echo = true, $options = [])
    {
        $pre  = '';
        $post = '';

        if ($language === 'sql') {
            if ($data instanceof QueryBuilder || $data instanceof Query) {
                if ($data instanceof QueryBuilder) {
                    $data = $data->getQuery();
                }

                $parameters = [];
                foreach ($data->getParameters() as $id => $parameter) {
                    $pval = '';
                    if ($parameter->getValue() instanceof \DateTime) {
                        $pval = $parameter->getValue()->format('d/m/Y');
                    } elseif (is_object($parameter->getValue())) {
                        if (method_exists($parameter->getValue(), 'getId')) $pval .= '<strong>' . $parameter->getValue()->getId() . '</strong>';
                        if (method_exists($parameter->getValue(), '__toString')) $pval .= ' : ' . (string)$parameter->getValue();
                        if ($pval == '') $pval = get_class($parameter->getValue());
                    } else {
                        $pval = (string)$parameter->getValue();
                    }
                    $parameters[$parameter->getName() . ' (' . $id . ')'] = $pval;
                }
                $data = $data->getSQL();
            } elseif (isset($options['parameters'])) {
                $parameters = $options['parameters'];
            } else {
                $parameters = [];
            }
            if (!empty($parameters)) {
                $post .= '<table class="table table-bordered table-condensed" style="width:auto"><tr><th>Paramètre</th><th>Valeur</th></tr>';
                foreach ($parameters as $key => $value) {
                    if (is_array($value)) $value = implode(', ', $value);
                    if ($value instanceof \DateTime) $value = $value->format('d/m/Y');
                    $post .= '<tr><td>' . $key . '</td><td>' . $value . '</td></tr>';
                }
                $post .= '</table>';
            }
            if (self::includeSqlFormatter()) {
                $data = \SqlFormatter::format($data, false);
            }
        } elseif ($language === 'php') {
            if (!is_string($data)) $data = var_export($data, true);
            $patterns = [
                "/array \(/"                       => '[',
                "/^([ ]*)\)(,?)$/m"                => '$1]$2',
                "/=>[ ]?\n[ ]+\[/"                 => '=> [',
                "/([ ]*)(\'[^\']+\') => ([\[\'])/" => '$1$2 => $3',
            ];
            $data     = preg_replace(array_keys($patterns), array_values($patterns), $data);
        }

        $geshi = new \GeSHi($data, $language);
        $geshi->set_overall_class('geshi');
        $geshi->set_header_type(GESHI_HEADER_PRE);
        if (isset($options['show-line-numbers']) && $options['show-line-numbers']) {
            $geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
            $geshi->set_line_style('background: white');
        }
        if ($echo) {
            echo $pre . $geshi->parse_code() . $post;
        } else {
            return $pre . $geshi->parse_code() . $post;
        }
    }



    /**
     * retourne le strict nom de classe de la classe (sans son namespace)
     *
     * @param string $class nom de classe, avec son namespace
     *
     * @return string
     */
    public static function classClassname(string $class): string
    {
        $pos = strrpos($class, '\\');

        return substr($class, $pos + 1);
    }



    /**
     * Part du nom complet de la classe (avec son namespace donc)
     * Enlève le début correspondant à $namespace s'il a été trouvé
     *
     * @param string $classname nom de classe, avec son namespace
     *
     * @return string
     */
    public static function classTruncated(string $class, string $namespace): string
    {
        if (empty($namespace)) return $class;
        if (0 === strpos($class, $namespace)) {
            return substr($class, strlen($namespace) + strlen('\\'));
        }

        return $class;
    }



    /**
     * Convertit une classe (avec son namespace au besoin) en nom de fichier correspondant
     *
     * @param string $class
     *
     * @return string
     */
    public static function classFilename(string $class): string
    {
        $module = self::classModule($class, self::CLASS_MODULE_CONFIG);

        $moduleSrc = $module['relPath'] . '/src/' . $module['name'];
        if (!file_exists($moduleSrc)) {
            $moduleSrc = $module['relPath'] . '/src';
        }
        if (!file_exists($moduleSrc)) {
            $moduleSrc = $module['relPath'];
        }

        $file = $class;
        if (0 === strpos($file, $module['name'])) {
            $file = substr($file, strlen($module['name']));
        }
        $file = str_replace('\\', '/', $file) . '.php';

        return $moduleSrc . $file;
    }



    /**
     * Convertit une classe (avec son namespace au besoin) en CamelCase pour servir de variable ou de nom de méthode
     *
     * @param string $classname
     *
     * @return string
     */
    public static function classCamelCase(string $classname): string
    {
        return str_replace('\\', '', $classname);
    }



    /**
     * Retourne le namespace de la classe
     *
     * @param string $class nom complet de classe
     *
     * @return string
     */
    public static function classNamespace(string $class): string
    {
        $pos = strrpos($class, '\\');

        return substr($class, 0, $pos);
    }



    /**
     * Retourne le module de la classe à partir de son namespace
     *
     * @param string $class      nom complet de classe
     * @param int    $typeReturn = [CLASS_MODULE_NAME => Nom du module, CLASS_MODULE_NAMESPACE => Namespace du module, CLASS_MODULE_CONFIG => array de config du module]
     *
     * @return string|array|null
     */
    public static function classModule(string $class, int $typeReturn = self::CLASS_MODULE_NAME)
    {
        $modules = self::introspection()->getModules(true);

        foreach ($modules as $module) {
            if (0 === strpos($class, $module['namespace'])) {
                switch ($typeReturn) {
                    case self::CLASS_MODULE_NAME:
                        return $module['name'];
                    case self::CLASS_MODULE_NAMESPACE:
                        return $module['namespace'];
                    case self::CLASS_MODULE_CONFIG:
                        return $module;
                }
            }
        }

        return null;
    }



    /**
     * Retourne la racine du chemin de la classe
     * Exemple : Service/PlafondService retournera Service
     *
     * @param string $class
     *
     * @return string
     */
    public static function classRoot(string $class): string
    {
        if (false !== strpos($class, '\\')) {
            return substr($class, 0, strpos($class, '\\'));
        } else {
            return '';
        }
    }



    /**
     * @param string $class
     *
     * @return string|null
     */
    public static function classType(string $class): ?string
    {
        $subClassTypes = [
            \Laminas\Form\Form::class                                    => 'Form',
            \Laminas\Form\Fieldset::class                                => 'Fieldset',
            \Laminas\View\Helper\HelperInterface::class                  => 'ViewHelper',
            \Laminas\Mvc\Controller\AbstractController::class            => 'Controller',
            \Laminas\Validator\ValidatorInterface::class                 => 'Validator',
            \Laminas\Hydrator\HydratorInterface::class                   => 'Hydrator',
            \Laminas\Permissions\Acl\Assertion\AssertionInterface::class => 'Assertion',

        ];

        $types = [
            'Connecteur'          => 'Connecteur',
            'Service'             => 'Service',
            'Entity'              => 'Entity',
            'Provider'            => 'Provider',
            'Processus'           => 'Processus',
            'Proxy'               => 'Proxy',
            'Fieldset'            => 'Fieldset',
            'Form'                => 'Form',
            'ViewHelper'          => 'ViewHelper',
            'View\\Helper'        => 'ViewHelper',
            'Controller'          => 'Controller',
            'Validator'           => 'Validator',
            'Hydrator'            => 'Hydrator',
            'Assertion'           => 'Assertion',
            'ORM\Event\Listeners' => 'Listener',
        ];

        if (class_exists($class)) {
            $rc = new \ReflectionClass($class);
            foreach ($subClassTypes as $parent => $type) {
                if ($rc->isSubclassOf($parent)) {
                    return $type;
                }
            }

            $entities = self::introspection()->getDbEntities();
            if (isset($entities[$class])) {
                return 'Entity';
            }
        }

        /* Au cas où ça n'existe pas encore, parsing à partir du nom et du namespace complet */
        foreach ($types as $key => $type) {
            if (false !== strpos($class, '\\' . $key . '\\')) {
                return $type;
            }
        }

        return null;
    }



    /**
     * Retourne la déclaration d'une méthode dans un bloc de commentaire avec "@method"
     *
     * @param \ReflectionMethod $method
     *
     * @return string
     */
    public static function getMethodDocDeclaration(\ReflectionMethod $method, $name = null, $defaultReturn = null)
    {
        $return = $defaultReturn;

        $parameters = $method->getParameters();
        $docComment = explode("\n", $method->getDocComment());
        foreach ($docComment as $docLine) {
            if (false !== strpos($docLine, '@return')) {
                $returnType = trim(substr($docLine, strpos($docLine, '@return') + strlen('@return ')));
                if ('string' == $returnType) $return = 'string';
                if ('integer' == $returnType) $return = 'integer';
                if ('boolean' == $returnType) $return = 'boolean';
                if ('float' == $returnType) $return = 'float';
            }
        }

        $params = '';
        foreach ($parameters as $parameter) {
            if ($params != '') {
                $params .= ', ';
            }
            if ($parameter->hasType()) {
                if ($parameter->getType()->allowsNull()) {
                    $params .= '?';
                }
                $type = $parameter->getType();
                if ($type instanceof \ReflectionUnionType){
                    $first = true;
                    foreach($type->getTypes() as $pt){
                        if (!$first) $params .= '|';
                        if (class_exists($pt->getName())) {
                            $params .= '\\';
                        }
                        $params .= (string)$pt->getName();
                        $first = false;
                    }
                    $params .= ' ';
                }else{
                    if (class_exists($type->getName())) {
                        $params .= '\\';
                    }
                    $params .= (string)$type->getName() . ' ';
                }
            }

            $params .= '$' . $parameter->getName();

            if ($parameter->isDefaultValueAvailable()) {
                $defaultValue = $parameter->getDefaultValue();
                if (null === $defaultValue) {
                    $params .= ' = null';
                } elseif ([] === $defaultValue) {
                    $params .= ' = []';
                } else {
                    $params .= ' = ' . var_export($defaultValue, true);
                }
            }
        }

        if (!$name) $name = $method->getName();
        $name = lcfirst($name);

        if ($return) $return .= ' ';

        return "@method $return$name($params)";
    }



    /**
     * @return EntityManager
     */
    public static function getEntityManager($name = 'doctrine.entitymanager.orm_default')
    {
        if (self::getContainer()->has($name)) {
            return self::getContainer()->get($name);
        } else {
            return null;
        }
    }



    /**
     * Retourne l'auteur correspondant au profil actuellement connecté
     *
     * @return string
     */
    public static function getAuthor($default = 'UnicaenCode')
    {
        try {
            /* @var $config ConfigService */
            $config = self::getContainer()->get(ConfigService::class);
            $author = $config->getAuthor();

            return $author;
        } catch (\Exception $e) {
        }
        $authenticationService = self::getContainer()->get('Laminas\Authentication\AuthenticationService');
        if ($authenticationService->hasIdentity()) {
            $identity = $authenticationService->getIdentity();
            if (isset($identity['ldap'])) {
                return $identity['ldap']->getDisplayName() . ' <' . str_replace('@', ' at ', $identity['ldap']->getMail()) . '>';
            }
            if (isset($identity['db'])) {
                return $identity['db']->getDisplayName() . ' <' . str_replace('@', ' at ', $identity['db']->getEmail()) . '>';
            }
        }
    }



    private static function includeSqlFormatter()
    {
        try {
            $config = self::getContainer()->get(ConfigService::class);
            /* @var $config Config */
        } catch (\Exception $e) {
            return false;
        }

        if ($file = $config->getSqlFormatterFile()) {
            if (file_exists($file)) {
                include_once $file;

                return true;
            }
        }

        return false;
    }

}





class EchoSQLLogger implements \Doctrine\DBAL\Logging\SQLLogger
{
    /**
     * display
     *
     * @var boolean
     */
    public $display;

    /**
     *
     * @var integer
     */
    static public $queries = [];



    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, array $params = null, array $types = null)
    {
        self::$queries[] = ['sql' => $sql, 'params' => $params];
        if ($this->display) {
            Util::dumpBacktrace(false, false);
            Util::highlight($sql, 'sql', true, ['parameters' => $params]);
        }
    }



    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
    }
}
