<?php

namespace UnicaenCode\Form;

use UnicaenCode\Util;
use Laminas\Form\Element;
use Laminas\Form\Form;

class FormMaker extends \Laminas\Form\Form
{
    protected array $params = [];



    public function addHidden(string $name, ?string $value = null): self
    {
        $options = [
            'type'       => 'Hidden',
            'name'       => $name,
            'attributes' => ['value' => $this->params[$name] ?? $value],
        ];
        $this->add($options);

        return $this;
    }



    public function addText(string $name, string $label, $value = null): self
    {
        $options = [
            'type'    => 'Text',
            'name'    => $name,
            'options' => [
                'label' => $label,
            ],
        ];

        $value = $this->params[$name] ?? $value;
        if ($value !== null) {
            $options['attributes']['value'] = $value;
        }

        $this->add($options);

        return $this;
    }



    public function addCheckbox(string $name, string $label, bool $value = false): self
    {
        $options = [
            'type'    => 'Checkbox',
            'name'    => $name,
            'options' => [
                'label' => $label,
            ],
        ];

        $value = $this->params[$name] ?? $value;
        if ($value !== null) {
            $options['attributes']['value'] = $value;
        }

        $this->add($options);

        return $this;
    }



    public function addSubmit(string $name, $value = null): self
    {
        $options = [
            'type'       => 'Submit',
            'name'       => $name,
            'attributes' => [
                'value' => $this->params[$name] ?? $value,
                'class' => 'btn btn-primary',
            ],
        ];

        $this->add($options);

        return $this;
    }



    public function addSelect(string $name, string $label, array $options, $value = null): self
    {
        $select = new Element\Select($name);
        $select->setLabel($label);
        $select->setValueOptions($options);
        $select->setValue($this->params[$name] ?? $value);

        $this->add($select);

        return $this;
    }



    public function addSelectModule(string $name, string $label, ?string $value = 'Application'): self
    {
        $options = [
            'module' => [
                'label'   => 'Module',
                'options' => [],
            ],
            'vendor' => [
                'label'   => 'Vendor',
                'options' => [],
            ],
        ];
        $modules = Util::introspection()->getModules(true);
        ksort($modules);
        foreach ($modules as $mName => $module) {
            if ($module['inVendor']) {
                $options['vendor']['options'][$mName] = $mName;
            } else {
                $options['module']['options'][$mName] = $mName;
            }
        }

        return $this->addSelect($name, $label, $options, $value);
    }



    public function addSelectEntity(string $name, string $label, ?string $namespace = null, ?string $value = null): self
    {
        $options  = [];
        $entities = Util::introspection()->getDbEntities(namespace: $namespace);

        ksort($entities);
        foreach ($entities as $entity) {
            if ($namespace && 0 === strpos($entity, $namespace)) {
                $entity = substr($entity, strlen($namespace) + 1);
            }
            $options[$entity] = $entity;
        }

        return $this->addSelect($name, $label, $options, $value);
    }



    public function init(array $params = [])
    {
        $this->params = $params;
    }



    public function getParams(): array
    {
        $params = $this->params;
        foreach ($this->elements as $name => $element) {
            if ($element instanceof Element\Checkbox) {
                $params[$name] = (bool)$element->getValue();
            } else {
                $params[$name] = $element->getValue();
            }
        }

        return $params;
    }



    public static function createFromArray(array $elements)
    {
        $form = new self();
        foreach ($elements as $name => $e) {
            if (is_array($e)) {
                $type  = strtolower($e['type'] ?? 'text');
                $label = $e['label'] ?? 'Label';
                $value = $e['value'] ?? null;
            } else {
                continue;
            }

            switch ($type) {
                case 'hidden':
                    $form->addHidden($name, $value);
                break;
                case 'text':
                    $form->addText($name, $label, $value);
                break;
                case 'checkbox':
                    $form->addCheckbox($name, $label, (bool)$value);
                break;
                case 'select':
                    $form->addSelect($name, $label, $e['options'] ?? [], $value);
                break;
                case 'selectmodule':
                    $form->addSelectModule($name, $label, $value);
                break;
                case 'selectentity':
                    $form->addSelectEntity($name, $label, $e['namespace'] ?? null, $value);
                break;
            }
        }

        return $form;
    }



    public function hasSaisieElements(): bool
    {
        foreach($this->elements as $element){
            /** @var $element Element */
            if (!($element instanceof Element\Submit || $element instanceof Element\Hidden)){
                return true;
            }
        }
        
        return false;
    }
    
    

    public function getParamValue(string $name)
    {
        if (!isset($this->elements[$name])) {
            return null;
        }
        $element = $this->get($name);

        if ($element instanceof Element\Checkbox) {
            return (bool)$element->getValue();
        }
        
        return $element->getValue();
    }



    public function afficher()
    {
        $this->add([
            'type'       => 'Submit',
            'name'       => 'echo',
            'attributes' => [
                'value' => 'Afficher le code source',
                'class' => 'btn btn-primary',
            ],
        ]);

        $vhm = Util::getContainer()->get('ViewHelperManager');
        /* @var $vhm \Laminas\View\HelperPluginManager */
        $view = $vhm->getRenderer();

        ?>
        <style>

            form .form-control {
                margin-left: 10%;
                width: inherit;
            }

            form input[type='text'].form-control {
                margin-left: 10%;
                width: 30em;
            }

            form input[type='submit'].form-control {
                margin-left: 0%;

            }

        </style>
        <?php

        echo $view->form()->openTag($this->prepare());
        foreach ($this->getElements() as $element) {
            if (!($element->getName() == 'echo')) {
                echo $view->formControlGroup($element);
            }
        }
        echo $view->formSubmit($this->get('echo'));
        echo $view->form()->closeTag();
    }
}