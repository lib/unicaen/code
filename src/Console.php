<?php

namespace UnicaenCode;

class Console
{
    const COLOR_BLACK        = '0;30';
    const COLOR_DARK_GRAY    = '1;30';
    const COLOR_BLUE         = '0;34';
    const COLOR_LIGHT_BLUE   = '1;34';
    const COLOR_GREEN        = '0;32';
    const COLOR_LIGHT_GREEN  = '1;32';
    const COLOR_CYAN         = '0;36';
    const COLOR_LIGHT_CYAN   = '1;36';
    const COLOR_RED          = '0;31';
    const COLOR_LIGHT_RED    = '1;31';
    const COLOR_PURPLE       = '0;35';
    const COLOR_LIGHT_PURPLE = '1;35';
    const COLOR_BROWN        = '0;33';
    const COLOR_YELLOW       = '1;33';
    const COLOR_LIGHT_GRAY   = '0;37';
    const COLOR_WHITE        = '1;37';

    const BG_BLACK      = '40';
    const BG_RED        = '41';
    const BG_GREEN      = '42';
    const BG_YELLOW     = '43';
    const BG_BLUE       = '44';
    const BG_MAGENTA    = '45';
    const BG_CYAN       = '46';
    const BG_LIGHT_GRAY = '47';



    public function check(array $prerequis)
    {
        $len = 60;
        $res = true;

        $this->println("Contrôle des prérequis à l'exécution du script", self::COLOR_LIGHT_CYAN);
        $this->println($this->strPad('Commande (description éventuelle)', $len) . "Résultat");
        $this->println($this->strPad('----------------------', $len) . "--------");
        foreach ($prerequis as $command => $desc) {
            if (is_array($desc)) {
                extract($desc);
            } else {
                $description = $desc;
                $facultatif  = false;
            }

            $return = null;
            exec('command -v ' . $command, $null, $result);
            $passed = ($result == 0);

            $this->print($this->strPad($command . ($description ? " ($description)" : ''), $len));
            if ($passed) {
                $this->println('Commande trouvée', self::COLOR_LIGHT_GREEN);
            } elseif ($facultatif) {
                $this->println('Manquante, facultative', self::COLOR_LIGHT_PURPLE);
            } else {
                $this->println('Commande manquante', self::COLOR_LIGHT_RED);
                $res = false;
            }
        }

        if (!$res) {
            $this->printDie('Un ou plusieurs prérequis nécessaires ne sont pas présents sur cette machine. Merci de les installer avant de poursuivre l\'installation.');
        }

        return $res;
    }



    public function printMainTitle($title, $spaces = 1)
    {
        $pstr = str_repeat(' ', $spaces);
        $t    = $pstr . $title . $pstr;

        $len = mb_strlen($t);

        echo '╔' . str_repeat('═', $len) . "╗\n";
        echo '║' . str_repeat(' ', $len) . "║\n";
        echo "║" . $t . "║\n";
        echo '║' . str_repeat(' ', $len) . "║\n";
        echo '╚' . str_repeat('═', $len) . "╝\n\n";
    }



    public function print($text, $color = null, $bgColor = null)
    {
        if ($bgColor) $bgColor = ';' . $bgColor;

        if (!$color && !$bgColor) {
            echo $text;
        } else {
            echo "\e[$color$bgColor" . "m$text\e[0m";
        }
    }



    public function println($text, $color = null, $bgColor = null)
    {
        $this->print($text, $color, $bgColor);
        echo "\n";
    }



    public function gestExitCode($code)
    {
        if (0 == $code) return;

        $this->printDie("Une erreur ($code) est survenue. Le script est stoppé");
    }



    public function printDie($text)
    {
        $this->println($text, self::COLOR_LIGHT_RED);
        $this->println(' -- FIN Prématurée de l\'exécution du script -- ', null, self::BG_RED);
        die("\n");
    }



    public function getArg($index = null)
    {
        $args = isset($_SERVER['argv']) ? $_SERVER['argv'] : [];

        if (null === $index) return $args;

        if (isset($args[$index])) {
            return $args[$index];
        } else {
            return null;
        }
    }



    public function getInput()
    {
        return trim(fgets(STDIN));
    }



    public function getSilentInput()
    {
        if (preg_match('/^win/i', PHP_OS)) {
            $vbscript = sys_get_temp_dir() . 'prompt_password.vbs';
            file_put_contents(
                $vbscript, 'wscript.echo(InputBox("'
                . addslashes('')
                . '", "", "password here"))');
            $command  = "cscript //nologo " . escapeshellarg($vbscript);
            $password = rtrim(shell_exec($command));
            unlink($vbscript);

            return $password;
        } else {
            $command = "/usr/bin/env bash -c 'echo OK'";
            if (rtrim(shell_exec($command)) !== 'OK') {
                trigger_error("Can't invoke bash");

                return;
            }
            $command  = "/usr/bin/env bash -c 'read -s -p \""
                . addslashes('')
                . "\" mypassword && echo \$mypassword'";
            $password = rtrim(shell_exec($command));
            echo "\n";

            return $password;
        }
    }



    public function exec($command, $autoDisplay = true)
    {
        if (is_array($command)) {
            $command = implode(';', $command);
        }

        exec($command, $output, $return);
        if ($autoDisplay) {
            echo implode("\n", $output) . "\n";
        }
        $this->gestExitCode($return);

        return $output;
    }



    public function passthru($command)
    {
        if (is_array($command)) {
            $command = implode(';', $command);
        }

        passthru($command, $returnVar);
        $this->gestExitCode($returnVar);

        return $returnVar;
    }



    public function strPad($input, $padLength = null, $padString = ' ')
    {
        return utf8_encode(str_pad(utf8_decode($input), $padLength, $padString));
    }
}