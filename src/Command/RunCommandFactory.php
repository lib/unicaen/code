<?php

namespace UnicaenCode\Command;

use Psr\Container\ContainerInterface;
use UnicaenCode\Service\ConfigService;
use UnicaenCode\Util;


/**
 * Description of RunCommandFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class RunCommandFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return RunCommand
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): RunCommand
    {
        Util::setContainer($container);

        $command = new RunCommand;
        $command->setServiceConfig($container->get(ConfigService::class));

        return $command;
    }
}