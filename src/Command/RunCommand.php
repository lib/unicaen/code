<?php

namespace UnicaenCode\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenCode\Service\Traits\ConfigServiceAwareTrait;
use UnicaenCode\Util;

/**
 * Description of RunCommand
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class RunCommand extends Command
{
    use ConfigServiceAwareTrait;

    protected function configure(): void
    {
        $this->setDescription('... description à adapter ...')
            ->addArgument(
                'arguments',
                InputArgument::IS_ARRAY,
                'Any arguments that you want to pass to this command'
            );
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Récupération des arguments fournis
        $arguments = $input->getArgument('arguments');

        $params    = [];
        $variables = [];

        foreach ($arguments as $value) {
            if (false !== ($eq = strpos($value, '='))) {
                $variables[substr($value, 0, $eq)] = substr($value, $eq + 1);
            } else {
                $params[] = $value;
            }
        }

        $viewName = $params[0];
        $viewFile = $this->getViewFile($viewName);

        Util::$currentView = $viewName;
        Util::$inputParams = $variables;

        if ($viewFile){
            $container = Util::getContainer();
            extract($variables);

            require $viewFile;
        }else{
            throw new \Exception('Script '.$viewName.' Introuvable');
        }

        return Command::SUCCESS;
    }



    private function getViewFile($viewName)
    {
        $viewDirs = $this->getServiceConfig()->getViewDirs();
        $viewFile = null;
        foreach( $viewDirs as $viewDir ){
            if (file_exists($viewDir.'/'.$viewName.'.php')){
                $viewFile = $viewDir.'/'.$viewName.'.php';
                break;
            }
        }

        return $viewFile;
    }
}