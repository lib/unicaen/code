<?php

namespace UnicaenCode\Service;

use UnicaenCode\ClassFileLocator;
use UnicaenCode\Manager\FormManager;
use UnicaenCode\Manager\HydratorManager;
use UnicaenCode\Manager\ServiceManager;
use UnicaenCode\Service\Traits\ConfigServiceAwareTrait;
use UnicaenCode\Util;
use UnicaenCode\Manager\ViewHelperManager;
use Laminas\ModuleManager\ModuleManager;

/**
 *
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class IntrospectionService
{
    use ConfigServiceAwareTrait;


    public function getTraitParams(string $class): array
    {
        $params = [
            'class' => $class,
            'aware' => false,
        ];

        $rc       = new \ReflectionClass($class);
        $filename = $rc->getFileName();

        $module = Util::classModule($class, util::CLASS_MODULE_CONFIG);

        if (0 === strpos($filename, $module['absPath'])) {
            $filename = substr($filename, strlen($module['absPath']));
        }
        $params['filename'] = $filename;
        $params['module']   = $module['name'];

        if (substr($class, -strlen('AwareTrait')) !== 'AwareTrait') {
            return $params;
        } else {
            $params['aware']  = true;
            $params['subDir'] = strpos($class, '\\Traits\\') !== false;
        }

        $ms               = $rc->getMethods();
        $params['getter'] = null;
        $params['setter'] = null;
        foreach ($ms as $m) {
            if (0 === strpos($m->getName(), 'get')) {
                $params['getter'] = $m->getName();
                if ($m->getReturnType()) {
                    $params['targetClass'] = $m->getReturnType()->getName();
                }
            } elseif (0 === strpos($m->getName(), 'set')) {
                $params['setter'] = $m->getName();
            }
        }

        $attrs = $rc->getProperties();
        if (1 === count($attrs)) {
            $attr               = $attrs[0];
            $params['property'] = $attr->getName();
            if (!isset($params['targetClass'])) {
                $type = $attr->getType();
                if ($type) {
                    $params['targetClass'] = $type->getName();
                }
            }
        }

        if (!isset($params['targetClass'])) {
            $targetClass = substr($class, 0, -strlen('AwareTrait'));
            if ($params['subDir']) {
                $targetClass = str_replace('\\Traits\\', '\\', $targetClass);
            }
            if (class_exists($targetClass)) {
                $params['targetClass'] = $targetClass;
            }
        }

        if (!isset($params['targetClass'])) {
            $params['targetClass'] = null;
        }

        return $params;
    }



    public function getInterfaceParams(string $class): array
    {
        $params = [
            'class' => $class,
            'aware' => false,
        ];

        $rc       = new \ReflectionClass($class);
        $filename = $rc->getFileName();

        $module = Util::classModule($class, util::CLASS_MODULE_CONFIG);

        if (0 === strpos($filename, $module['absPath'])) {
            $filename = substr($filename, strlen($module['absPath']));
        }
        $params['filename'] = $filename;
        $params['module']   = $module['name'];

        if (substr($class, -strlen('AwareInterface')) !== 'AwareInterface') {
            return $params;
        } else {
            $params['aware']  = true;
            $params['subDir'] = strpos($class, '\\Interfaces\\') !== false;
        }

        $ms               = $rc->getMethods();
        $params['getter'] = null;
        $params['setter'] = null;
        foreach ($ms as $m) {
            if (0 === strpos($m->getName(), 'get')) {
                $params['getter'] = $m->getName();
                if ($m->getReturnType()) {
                    $params['targetClass'] = $m->getReturnType()->getName();
                }
            } elseif (0 === strpos($m->getName(), 'set')) {
                $params['setter'] = $m->getName();
            }
        }

        if (!isset($params['targetClass'])) {
            $targetClass = substr($class, 0, -strlen('AwareInterface'));
            if ($params['subDir']) {
                $targetClass = str_replace('\\Interfaces\\', '\\', $targetClass);
            }
            if (class_exists($targetClass)) {
                $params['targetClass'] = $targetClass;
            }
        }

        if (!isset($params['targetClass'])) {
            $params['targetClass'] = null;
        }

        return $params;
    }



    /**
     * Retourne la liste des services accessibles depuis le ServiceManager et correspondant aux critères suivants :
     * Les chaînes retournées sont les noms de classe des services
     *
     * @param null|string $namespace     Les classes retournées devront faire partie du namespace spécifié
     * @param bool        $includeVendor Détermine si on veut aussi des entités venant du répertoire vendor ou bien uniquement de notre application
     *
     * @return string[]
     */
    public function getServices(?string $namespace = null, bool $includeVendor = false): array
    {
        $manager = new ServiceManager(Util::getContainer());
        $result  = $manager->getClasses();

        return $this->filterClasses($result, $namespace, $includeVendor);
    }



    public function getClasses(?string $namespace = null, bool $includeVendor = false): array
    {
        $cti = $this->getCTI($namespace, $includeVendor);
        foreach ($cti as $k => $c) {
            if (@trait_exists($c) || @interface_exists($c)) {
                continue;
            }
            if (!@class_exists($c)) {
                throw new \Exception('La classe ' . $c . ' n\existe pas ou bien elle elle est introuvable');
            }
        }

        return array_values($cti);
    }



    public function getTraits(?string $namespace = null, bool $includeVendor = false): array
    {
        $cti = $this->getCTI($namespace, $includeVendor);
        foreach ($cti as $k => $t) {
            if (!trait_exists($t)) {
                unset($cti[$k]);
            }
        }

        return array_values($cti);
    }



    public function getInterfaces(?string $namespace = null, bool $includeVendor = false): array
    {
        $cti = $this->getCTI($namespace, $includeVendor);
        foreach ($cti as $k => $i) {
            if (!interface_exists($i)) {
                unset($cti[$k]);
            }
        }

        return array_values($cti);
    }



    private function getCTI(?string $namespace = null, bool $includeVendor = false): array
    {
        if ($namespace) $includeVendor = true;

        $path = getcwd();
        if (!$includeVendor) {
            $path .= '/module';
        }

        $l = new ClassFileLocator($path);

        $map = [];
        foreach ($l as $file) {
            foreach ($file->getClasses() as $class) {
                $ok = true;
                if (!$includeVendor) {
                    $module = Util::classModule($class, Util::CLASS_MODULE_CONFIG);
                    if (null === $module) {
                        $ok = false;
                    } else {
                        $ok = !$module['inVendor'];
                    }
                }

                if ($ok && (!$namespace || str_starts_with($class, $namespace))) {
                    $map[] = $class;
                }
            }
        }
        sort($map);

        return array_unique($map);
    }



    public function getViews(string $modulename): array
    {
        $module       = $this->getModule($modulename);
        $templatePath = $module['absPath'] . '/' . $module['viewDir'];

        if (!file_exists($templatePath)) {
            return [];
        }

        $rdi = new \RecursiveDirectoryIterator(
            $templatePath,
            \RecursiveDirectoryIterator::FOLLOW_SYMLINKS | \RecursiveDirectoryIterator::SKIP_DOTS
        );
        $rii = new \RecursiveIteratorIterator($rdi, \RecursiveIteratorIterator::LEAVES_ONLY);

        $views = [];
        foreach ($rii as $file) {
            if (strtolower($file->getExtension()) != 'phtml') {
                continue;
            }

            $views[] = substr($file->getPathname(), strlen($templatePath . '/'), -6);
        }

        return $views;
    }



    public function getSrcs(string $modulename, bool $absPaths = false): array
    {
        $module       = $this->getModule($modulename);
        $templatePath = $module['absPath'] . '/' . $module['srcDir'];

        if (!file_exists($templatePath)) {
            return [];
        }

        $rdi = new \RecursiveDirectoryIterator(
            $templatePath,
            \RecursiveDirectoryIterator::FOLLOW_SYMLINKS | \RecursiveDirectoryIterator::SKIP_DOTS
        );
        $rii = new \RecursiveIteratorIterator($rdi, \RecursiveIteratorIterator::LEAVES_ONLY);

        $srcs = [];
        foreach ($rii as $file) {
            if (strtolower($file->getExtension()) != 'php') {
                continue;
            }

            if ($absPaths) {
                $srcs[] = $file->getPathname();
            } else {
                $srcs[] = substr($file->getPathname(), strlen($templatePath . '/'));
            }

        }

        return $srcs;
    }



    /**
     * Retourne la liste des aides de vues accessibles depuis le ServiceManager et correspondant aux critères suivants :
     * Les chaînes retournées sont les noms de classe des aides de vues
     *
     * @param null|string $namespace     Les classes retournées devront faire partie du namespace spécifié
     * @param bool        $includeVendor Détermine si on veut aussi des entités venant du répertoire vendor ou bien uniquement de notre application
     *
     * @return string[]
     */
    public function getViewHelpers(?string $namespace = null, bool $includeVendor = false): array
    {
        $manager = new ViewHelperManager(Util::getContainer());
        $result  = $manager->getClasses();

        return $this->filterClasses($result, $namespace, $includeVendor);
    }



    /**
     * Retourne la liste des formulaires accessibles depuis le ServiceManager et correspondant aux critères suivants :
     * Les chaînes retournées sont les noms de classe des services
     *
     * @param null|string $namespace     Les classes retournées devront faire partie du namespace spécifié
     * @param bool        $includeVendor Détermine si on veut aussi des entités venant du répertoire vendor ou bien uniquement de notre application
     *
     * @return string[]
     */
    public function getForms(?string $namespace = null, bool $includeVendor = false): array
    {
        $manager = new FormManager(Util::getContainer());
        $result  = $manager->getClasses();

        return $this->filterClasses($result, $namespace, $includeVendor);
    }



    /**
     * Retourne la liste des entités utilisées par doctrine dans l'application
     *
     * @param null|string $namespace     Les classes retournées devront faire partie du namespace spécifié
     * @param bool        $includeVendor Détermine si on veut aussi des entités venant du répertoire vendor ou bien uniquement de notre application
     *
     * @return string[]
     */
    public function getDbEntities(?string $namespace = null, bool $includeVendor = false): array
    {
        $result = [];
        $metas  = Util::getEntityManager()->getMetadataFactory()->getAllMetadata();
        foreach ($metas as $meta) {
            $eclass          = $meta->getName();
            $result[$eclass] = $eclass;
        }
        asort($result);

        return $this->filterClasses($result, $namespace, $includeVendor);
    }



    private function filterClasses(array $classes, ?string $namespace = null, bool $includeVendor = false): array
    {
        if ($namespace && substr($namespace, -1) !== '\\') {
            $namespace .= '\\';
        }
        foreach ($classes as $name => $class) {
            if ($namespace && $namespace != '\\') {
                if (0 !== strpos($class, $namespace)) {
                    unset($classes[$name]);
                }
            }

            if ($includeVendor) {
                $module = Util::classModule($class, Util::CLASS_MODULE_CONFIG);
                if (isset($module['inVendor']) && !$module['inVendor']) {
                    unset($classes[$name]);
                }
            }
        }

        return $classes;
    }



    /**
     * Retourne la liste des modules de l'application avec des informations associées
     *
     * Retourne un tableau associatif de la forme :
     *
     * nom du module => [
     *      name => string : nom du module,
     *      absPath => string : chemin d'accès du module,
     *      relPath => string : chemin d'accès du module, relatif // au dossier de l'appli,
     *      inVendor => boolean : détermine si le module est dans /vendor ou non
     *      instance => instance du module (si nécessaire)
     * ]
     *
     * @param bool $includeVendor inclue les modules du répertoire vendor ou non
     *
     * @return array
     */
    public function getModules(bool $includeVendor = false): array
    {
        $moduleManager = Util::getContainer()->get('ModuleManager');
        /* @var $moduleManager ModuleManager */


        $modulesNames = $moduleManager->getModules();
        $modules      = [];
        foreach ($modulesNames as $module) {
            $mod = $this->getModule($module);

            if (!$mod['inVendor'] || $includeVendor) {
                $modules[$module] = $mod;
            }
        }

        return $modules;
    }



    public function getModule(string $name): array
    {
        $moduleManager = Util::getContainer()->get('ModuleManager');
        /* @var $moduleManager ModuleManager */

        $instance = $moduleManager->getModule($name);
        $rc       = new \ReflectionClass($instance);

        $appPath  = getcwd();
        $path     = dirname($rc->getFileName());
        $inVendor = 0 === strpos(str_replace($appPath . '/', '', $path), 'vendor');

        return [
            'name'      => $name,
            'namespace' => $rc->getNamespaceName(),
            'absPath'   => $path,
            'relPath'   => substr($path, strlen($appPath) + 1),
            'inVendor'  => $inVendor,
            'viewDir'   => 'view',
            'srcDir'    => 'src',
            'configDir' => 'config',
        ];
    }
}