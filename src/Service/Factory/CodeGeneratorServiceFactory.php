<?php

namespace UnicaenCode\Service\Factory;

use Psr\Container\ContainerInterface;
use UnicaenCode\Service\CodeGeneratorService;
use UnicaenCode\Service\ConfigService;

class CodeGeneratorServiceFactory
{

    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $service = new CodeGeneratorService;
        $service->setServiceConfig($container->get(ConfigService::class));

        return $service;
    }

}