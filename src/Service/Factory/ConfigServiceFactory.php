<?php

namespace UnicaenCode\Service\Factory;

use Psr\Container\ContainerInterface;
use UnicaenCode\Service\ConfigService;

class ConfigServiceFactory
{

    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $config = $container->get('Config');

        $service = new ConfigService($config);

        return $service;
    }

}