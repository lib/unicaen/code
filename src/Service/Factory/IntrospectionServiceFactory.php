<?php

namespace UnicaenCode\Service\Factory;

use Psr\Container\ContainerInterface;
use UnicaenCode\Service\ConfigService;
use UnicaenCode\Service\IntrospectionService;

class IntrospectionServiceFactory
{

    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $service = new IntrospectionService;
        $service->setServiceConfig($container->get(ConfigService::class));

        return $service;
    }

}