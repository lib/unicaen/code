<?php

namespace UnicaenCode\Service\Factory;

use Psr\Container\ContainerInterface;
use UnicaenCode\Service\CollectorService;
use UnicaenCode\Service\ConfigService;

class CollectorServiceFactory
{

    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        /** @var $urlVH Laminas\View\Helper\Url*/
        $urlVH = $container->get('ViewHelperManager')->get('url');

        $service = new CollectorService($urlVH);
        $service->setServiceConfig($container->get(ConfigService::class));

        return $service;
    }

}