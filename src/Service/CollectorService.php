<?php

namespace UnicaenCode\Service;

use Laminas\View\Helper\Url;
use Laminas\Mvc\MvcEvent;
use UnicaenCode\Service\Traits\ConfigServiceAwareTrait;
use Laminas\DeveloperTools\Collector\CollectorInterface;

/**
 * Collecteur de données UnicaenCode
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class CollectorService implements CollectorInterface
{
    use ConfigServiceAwareTrait;

    const NAME     = 'unicaen-code_collector';
    const PRIORITY = 150;

    protected Url $urlVH;



    public function __construct(Url $urlVH)
    {
        $this->urlVH = $urlVH;
    }



    public function getViews(): array
    {
        $viewDirs  = $this->getServiceConfig()->getViewDirs();
        $viewFiles = [];
        foreach ($viewDirs as $viewDir) {
            $files  = new \RecursiveDirectoryIterator(
                $viewDir,
                \RecursiveDirectoryIterator::SKIP_DOTS
            );
            $rFiles = new \RecursiveIteratorIterator($files);
            foreach ($rFiles as $file) {
                $file = $file->getPathname();
                if (str_starts_with($file, $viewDir)) {
                    $file = substr($file, strlen($viewDir));
                    if (str_starts_with($file, '/')) {
                        $file = substr($file, 1);
                    }
                    if (!str_starts_with($file, 'generator') && !str_starts_with($file, 'template')) {
                        $viewFiles[] = $file;
                    }
                }
            }
        }
        $viewFiles = array_unique($viewFiles);
        sort($viewFiles);
        $items = [];
        foreach ($viewFiles as $viewFile) {
            if (false !== strrpos($viewFile, '.php')) {
                $name         = substr($viewFile, 0, strrpos($viewFile, '.php'));
                $url          = $this->urlVH->__invoke('unicaen-code', ['view' => str_replace('/', '.', $name)]);
                $items[$name] = $url;
            }
        }
        return $items;
    }



    public function getItems(): array
    {
        $items = $this->getViews();

        $items['<hr />'] = null;

        $items['Icônes Unicaen'] = $this->urlVH->__invoke('icons');

        return $items;
    }



    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return self::class;
    }



    /**
     * {@inheritDoc}
     */
    public function getPriority()
    {
        return static::PRIORITY;
    }



    /**
     * {@inheritDoc}
     */
    public function collect(MvcEvent $mvcEvent)
    {

    }



    /**
     * {@inheritDoc}
     */
    public function __serialize()
    {
        return [];
    }



    /**
     * {@inheritDoc}
     */
    public function __unserialize($serialized)
    {

    }
}
