<?php

namespace UnicaenCode\Service;


/**
 * Service de configuration d'UnicaenCode
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class ConfigService
{
    /**
     * @var array
     */
    protected $globalConfig;

    protected $config;



    public function __construct(array $globalConfig)
    {
        $this->globalConfig = $globalConfig;
    }



    /**
     * Retourne la liste des répertoires de code
     *
     * @return mixed
     */
    public function getViewDirs()
    {
        $vd = $this->getConfig()['view-dirs'];
        usort($vd, function ($a, $b) { // UnicaenCode doit toujours arriver à la fin dans la fusion
            if (false !== strpos($a, 'vendor')) $a = 999; else $a = 0;
            if (false !== strpos($b, 'vendor')) $b = 999; else $b = 0;

            return $a - $b;
        });

        return $vd;
    }



    /**
     * Retourne la liste des modèles de code
     *
     * @return array
     */
    public function getTemplateDirs(): array
    {
        $td = $this->getConfig()['template-dirs'];
        usort($td, function ($a, $b) { // UnicaenCode doit toujours arriver à la fin dans la fusion
            if (false !== strpos($a, 'vendor')) $a = 999; else $a = 0;
            if (false !== strpos($b, 'vendor')) $b = 999; else $b = 0;

            return $a > $b ? 1 : 0;
        });

        return $td;
    }



    /**
     * Retourne la liste des dossiers de générateurs
     *
     * @return array
     */
    public function getGeneratorDirs(): array
    {
        $td = $this->getConfig()['generator-dirs'];
        usort($td, function ($a, $b) { // UnicaenCode doit toujours arriver à la fin dans la fusion
            if (false !== strpos($a, 'vendor')) $a = 999; else $a = 0;
            if (false !== strpos($b, 'vendor')) $b = 999; else $b = 0;

            return $a > $b ? 1 : 0;
        });

        return $td;
    }



    /**
     * Retourne le chemin vers lequel seront générés les fichiers (répertoire tmp par défaut)
     *
     * @return string
     */
    public function getGeneratorOutputDir(): string
    {
        return $this->getConfig()['generator-output-dir'];
    }



    public function getGeneratorCmd(): string
    {
        return $this->getConfig()['generator-cmd'];
    }



    /**
     * Retourne le chemin vers lequel seront générés les fichiers (répertoire tmp par défaut)
     *
     * @return array
     */
    public function getTriggers(): array
    {
        return $this->getConfig()['triggers'];
    }



    /**
     * Retourne le chemin vers lequel seront générés les fichiers (répertoire tmp par défaut)
     *
     * @return array
     */
    public function getGeneratorsFrom(): array
    {
        return $this->getConfig()['generators-from'];
    }



    /**
     * Retourne le fichier à inclure pour utiliser SqlFormatter
     *
     * @return string
     */
    public function getSqlFormatterFile()
    {
        return $this->getConfig()['sqlformatter-file'];
    }



    /**
     * Retourne le fichier à inclure pour utiliser Gehsi
     *
     * @return string
     */
    public function getGeshiFile()
    {
        return $this->getConfig()['geshi-file'];
    }



    /**
     * Retourne l'auteur courant
     *
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->getConfig()['author'] ?? 'UnicaenCode';
    }



    /**
     * Retourne la configuration globale de l'application
     *
     * @return array
     */
    public function getGlobalConfig()
    {
        return $this->globalConfig;
    }



    /**
     * Retourne la configuration d'UnicaenCode
     *
     * @return array
     */
    protected function getConfig()
    {
        if (!$this->config) {
            $this->config = $this->getGlobalConfig()['unicaen-code'];
        }

        return $this->config;
    }

}
