<?php

namespace UnicaenCode\Service;

use Exception;
use UnicaenCode\Form\FormMaker;
use UnicaenCode\Service\Traits\ConfigServiceAwareTrait;
use UnicaenCode\Util;

/**
 *
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class CodeGeneratorService
{
    const STATE_CONSOLE     = 'console';
    const STATE_WEB_DISPLAY = 'web-display';
    const STATE_WEB_GEN     = 'web-gen';
    const STATE_CODE        = 'code';

    use ConfigServiceAwareTrait;


    /**
     * @param array|string $params
     * @param array|bool   $input
     *
     * @return array
     */
    public function generer($params, $input = true): array
    {
        /* Récupération de l'input */
        if (true === $input) {
            if (Util::inConsole()) {
                $input = Util::$inputParams;
                $state = self::STATE_CONSOLE;
            } elseif (Util::isPost()) {
                $input = Util::$inputParams;
                $state = self::STATE_WEB_GEN;
            } else {
                $input = [];
                $state = self::STATE_WEB_DISPLAY;
            }
        } elseif (is_array($input)) {
            $state = self::STATE_CODE;
        } else {
            $input = [];
        }


        /* Récupération du générator */
        if (is_string($params)) {
            $params = $this->getGenerator($params);
        }


        /* Fusion de l'input dans les params */
        $oriParams = $params;
        $params    = [];
        foreach ($oriParams as $name => $value) {
            if (is_array($value)) {
                if (array_key_exists($name, $input)) {
                    $params[$name] = $input[$name];
                } else {
                    $params[$name] = $value['value'] ?? null;
                }
            } else {
                $params[$name] = $value;
            }
        }


        /* Création de la ligne de commande */
        $params['commandLine'] = $this->genCmd($oriParams, $params);


        /* Génération de paramètres supplémentaires */
        if ($state === self::STATE_CODE) {
            $params['expanded'] ??= true;
        }


        /* On affiche le titre */
        $title = $params['title'] ?? null;
        if ($title) switch ($state) {
            case self::STATE_CONSOLE:
//                    Util::getAuthor(); // sert à éviter un bug en console qui charge la session trop tard
                echo $title . "\n";
                break;
            case self::STATE_WEB_DISPLAY:
            case self::STATE_WEB_GEN:
                echo '<h1>' . $title . '</h1>';
                break;
        }


        /* On s'occupe du formulaire */
        if (in_array($state, [self::STATE_WEB_GEN, self::STATE_WEB_DISPLAY])) {
            $formParams = [];
            foreach ($oriParams as $name => $value) {
                if (is_array($value)) {
                    $formParams[$name]          = $value;
                    $formParams[$name]['value'] = $params[$name];
                }
            }
            $form = FormMaker::createFromArray($formParams);
            if ($form->hasSaisieElements()) {
                $form->afficher();
            }
        }


        /* Génération des paramètres finaux et rendu */
        if (!($state === self::STATE_WEB_DISPLAY && $form->hasSaisieElements())) {
            $params = $this->generate($params);
            $this->render($params);
        }


        /* Affichage éventuel de la ligne de commande */
        if ($state === self::STATE_WEB_GEN || (isset($form) && !$form->hasSaisieElements())) {
            ?>
            <h1>Création des fichiers depuis la ligne de commandes</h1>
            <div class="alert alert-warning">
                <p>Pour ajouter les fichiers listés ci-dessus à votre projet (hors exemples de configuration), lancez
                    depuis le répertoire de votre projet la
                    commande suivante :
                    <br/>
                    <code><?= $params['commandLine'] ?></code>

                </p>
            </div>
            <?php
        }

        return $params;
    }



    protected function generate($params): array
    {
        /* Passage du générateur */
        $generator = $params['generator'] ?? null;

        if (is_callable($generator)) {
            $params = $generator($params);
            unset($params['generator']);
        }


        /* On crée les sous-configs */
        foreach ($params as $key => $val) {
            if ((true === $val || '1' === $val) && $this->isGenerator($key)) {
                $this->generateFrom($params, $key);
            } elseif (is_array($val) && isset($val['generator'])) {
                $params[$key] = $this->generate($val);
            }
        }

        $params = $this->runTriggers($params);

        return $params;
    }



    protected function render(array $params)
    {
        foreach ($params as $p => $v) {
            if (is_array($v)) {
                $this->render($v);
            }
        }

        /* Génération du fichier final & rendu */
        $params = $this->makeCode($params);

        if (Util::inConsole()) {
            $this->write($params);
        } else {
            $this->echo($params);
        }
    }



    protected function genCmd(array $oriParams, array $params): string
    {
        $diff = [];
        foreach ($oriParams as $key => $oriVal) {
            if (is_callable($oriVal)) continue;
            if (is_array($oriVal)) $oriVal = $oriVal['value'] ?? null;
            if ($oriVal != $params[$key]) {
                $diff[$key] = $params[$key];
            }
        }

        $cmd      = $this->getServiceConfig()->getGeneratorCmd() . ' ';
        $viewName = Util::$currentView;
        $cmd      .= $viewName;
        if (!empty($diff)) {
            foreach ($diff as $k => $v) {
                if ($v === true) $v = 'true';
                if ($v === false) $v = 'false';

                $cmd .= ' ' . $k . '=' . escapeshellarg($v);
            }
        }

        return $cmd;
    }



    public function generateFrom(array &$params, string $key)
    {
        $subGenerator = $this->getGenerator($key);

        foreach ($subGenerator as $p => $v) {
            if (is_array($v)) {
                if (array_key_exists($p, $params)) {
                    $subGenerator[$p] = $params[$p];
                } else {
                    $subGenerator[$p] = $v['value'] ?? null;
                }
            }
        }
        $params[$key] = $this->generate($subGenerator);
    }



    public static function transferParams(array $params, array $subGenerator): array
    {

        foreach ($subGenerator as $param) {
            if (isset($params[$param]) && is_array($useVal)) $subGenerator[$param] = $params[$param];
        }

        return $subGenerator;
    }



    protected function runTriggers(array $params): array
    {
        $triggers = $this->getServiceConfig()->getTriggers();
        foreach ($triggers as $t) {
            $function = ($t['generator']) ?? '';
            unset($t['generator']);
            $run = true;
            if (!empty($t)) {
                foreach ($t as $pn => $pv) {
                    if (!isset($params[$pn])) {
                        $run = false;
                        break;
                    }
                    if (is_array($pv)) {
                        if (!in_array($params[$pn], $pv)) {
                            $run = false;
                            break;
                        }
                    } else {
                        if ($params[$pn] != $pv) {
                            $run = false;
                            break;
                        }
                    }
                }
            }
            if ($run) {
                $params = $function($params);
            }
        }

        return $params;
    }



    /**
     *
     * @throws Exception
     */
    protected function makeCode(array $params)
    {
        $template = $params['template'] ?? null;

        if (!$template) return $params;

        $code = $this->getTemplateCode($template);

        foreach ($params as $param => $value) {
            if (!is_array($value) && !is_object($value)) {
                $code = $this->codeSetVariable($code, $param, $value);
            }
        }

        $code = trim($code); // éviter les espaces inutiles en début et fin de fichier

        /* On débarasse le code de ses caractères inutiles !! */
        $code = explode(PHP_EOL, $code);
        foreach ($code as $i => $line) {
            if (trim($line)) {
                $code[$i] = rtrim($line);
            } else {
                $code[$i] = '';
            }
        }
        $code = implode(PHP_EOL, $code);

        $params['code'] = $code;

        return $params;
    }



    public function getGenerator(string $generator): array
    {
        $generatorDirs = $this->getServiceConfig()->getGeneratorDirs();
        foreach ($generatorDirs as $generatorDir) {
            $generatorFile = $generatorDir . '/' . $generator . '.php';
            if (file_exists($generatorFile)) break;
        }

        if (!$generatorFile) {
            throw new Exception('Le générateur ' . $generator . ' n\'a pas été trouvé dans les répertoires de générateurs d\'UnicaenCode');
        }

        return require($generatorFile);
    }



    public function isGenerator(string $generator)
    {
        $generatorDirs = $this->getServiceConfig()->getGeneratorDirs();
        foreach ($generatorDirs as $generatorDir) {
            $generatorFile = $generatorDir . '/' . $generator . '.php';
            if (file_exists($generatorFile)) break;
        }

        return file_exists($generatorFile);
    }



    public function getTemplateCode(string $template): string
    {
        $templateDirs = $this->getServiceConfig()->getTemplateDirs();
        $templateFile = null;
        foreach ($templateDirs as $templateDir) {
            $templateFile = $templateDir . '/' . $template . '.php';
            if (file_exists($templateFile)) break;
        }

        if (!$templateFile) {
            throw new Exception('Le modèle ' . $template . ' n\'a pas été trouvé dans les répertoires de modèles d\'UnicaenCode');
        }

        return file_get_contents($templateFile);
    }



    protected function codeSetVariable($code, $param, $value)
    {
        // Si c'est un tableau alors on concatène en string...
        if (is_array($param)) {
            $param = implode('', $param);
        }

        // application des paramètres simples
        $code = str_replace('<' . $param . '>', $value, $code);

        // Gestion des conditions
        $if = '<if ' . $param . '>';
        while (false !== ($begin = strpos($code, $if))) {
            $endIf = '<endif ' . $param . '>';

            $end = strpos($code, $endIf, $begin);

            if ($value) {
                $ifCode = ltrim(substr($code, $begin + strlen($if), $end - $begin - strlen($if)));
            } else {
                $ifCode = '';
            }

            $end += strlen($endIf);

            if (isset($code[$begin - 1]) && isset($code[$end])) {
                $b = trim($code[$begin - 1]);
                $e = trim($code[$end]);
                // ATTENTION : il n'y pas de support des retours chariot Windows (\n\r) car ils font 2 caractères ! ! !
                if ($b === '' && $e === '') $end++; // pour éviter les retours chariot ou espaces inutiles
            }

            $code = substr($code, 0, $begin) . $ifCode . substr($code, $end);
        }

        // Gestion des conditions
        $if = '<if ' . $param . ' notrim>';
        while (false !== ($begin = strpos($code, $if))) {
            $endIf = '<endif ' . $param . '>';

            $end = strpos($code, $endIf, $begin);

            if ($value) {
                $ifCode = substr($code, $begin + strlen($if), $end - $begin - strlen($if));
            } else {
                $ifCode = '';
            }

            $end += strlen($endIf);

            if (isset($code[$begin - 1]) && isset($code[$end])) {
                $b = trim($code[$begin - 1]);
                $e = trim($code[$end]);
                // ATTENTION : il n'y pas de support des retours chariot Windows (\n\r) car ils font 2 caractères ! ! !
                if ($b === '' && $e === '') $end++; // pour éviter les retours chariot ou espaces inutiles
            }

            $code = substr($code, 0, $begin) . $ifCode . substr($code, $end);
        }

        // restitution
        return $code;
    }



    /**
     * @param array $params
     *
     * @return $this
     */
    protected function echo(array $params): self
    {
        $expanded = $params['expanded'] ?? true;
        $filename = $params['filename'] ?? 'Code source (fichier sans nom)';
        $code     = $params['code'] ?? null;

        if (!$code) return $this;

        $write = $params['write'] ?? true;

        $id = uniqid('bloc_code_');
        ?>
        <a role="button" data-bs-toggle="collapse" href="#<?= $id ?>"
           aria-expanded="<?= $expanded ? 'true' : 'false' ?>" aria-controls="collapseExample">
            <?= $filename ?>
        </a>
        <?php if (!$write): ?>
        <span class="label label-warning">A TRAITER MANUELLEMENT</span>
    <?php endif; ?>
        <br/>
        <div class="collapse<?= $expanded ? ' in' : '' ?>" id="<?= $id ?>">
            <?php Util::highlight($code, 'php', true, ['show-line-numbers' => true]); ?>
        </div>
        <?php

        return $this;
    }



    /**
     * @param array $params
     *
     * @return $this
     */
    protected function write(array $params): self
    {
        $write    = $params['write'] ?? true;
        $filename = $params['filename'] ?? null;
        $code     = $params['code'] ?? null;

        if (!$filename) {
            return $this;
        }

        $outputDir = $params['outputDir'] ?? getcwd();
        $filename  = $outputDir . '/' . $filename;

        if (!$write) {
            echo 'A TRAITER MANUELLEMENT : ' . $filename . "\n";

            return $this;
        }

        $parts = explode('/', substr($filename, 1));
        array_pop($parts);
        $dir = '';
        foreach ($parts as $part) {
            if (!is_dir(($dir .= "/$part"))) {
                mkdir($dir);
                chmod($dir, 0777);
            }
        }

        file_put_contents($filename, $code);
        echo 'FICHIER ECRIT : ' . $filename . "\n";
        chmod($filename, 0777);

        return $this;
    }
}
