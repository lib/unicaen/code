<?php

namespace UnicaenCode\Service\Traits;

use UnicaenCode\Service\CollectorService;
use Common\Exception\RuntimeException;
use UnicaenCode\Util;

trait CollectorServiceAwareTrait
{
    /**
     * @var CollectorService
     */
    private $serviceCollector;



    /**
     *
     * @param CollectorService $serviceCollector
     *
     * @return self
     */
    public function setServiceCollector(CollectorService $serviceCollector)
    {
        $this->serviceCollector = $serviceCollector;

        return $this;
    }



    /**
     *
     * @return CollectorService
     * @throws \Common\Exception\RuntimeException
     */
    public function getServiceCollector()
    {
        return $this->serviceCollector;
    }

}