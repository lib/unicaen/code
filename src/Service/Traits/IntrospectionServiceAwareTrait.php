<?php

namespace UnicaenCode\Service\Traits;

use UnicaenCode\Service\IntrospectionService;
use Common\Exception\RuntimeException;
use UnicaenCode\Util;

trait IntrospectionServiceAwareTrait
{
    /**
     * @var IntrospectionService
     */
    private $serviceIntrospection;



    /**
     *
     * @param IntrospectionService $serviceIntrospection
     *
     * @return self
     */
    public function setServiceIntrospection(IntrospectionService $serviceIntrospection)
    {
        $this->serviceIntrospection = $serviceIntrospection;

        return $this;
    }



    /**
     *
     * @return IntrospectionService
     * @throws \Common\Exception\RuntimeException
     */
    public function getServiceIntrospection()
    {
        return $this->serviceIntrospection;
    }

}