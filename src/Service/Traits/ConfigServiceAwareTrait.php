<?php

namespace UnicaenCode\Service\Traits;

use UnicaenCode\Service\ConfigService;
use Common\Exception\RuntimeException;
use UnicaenCode\Util;

trait ConfigServiceAwareTrait
{
    /**
     * @var ConfigService
     */
    private $serviceConfig;



    /**
     *
     * @param ConfigService $serviceConfig
     *
     * @return self
     */
    public function setServiceConfig(ConfigService $serviceConfig)
    {
        $this->serviceConfig = $serviceConfig;

        return $this;
    }



    /**
     *
     * @return ConfigService
     * @throws \Common\Exception\RuntimeException
     */
    public function getServiceConfig()
    {
        return $this->serviceConfig;
    }

}