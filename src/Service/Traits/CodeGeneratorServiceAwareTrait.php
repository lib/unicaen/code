<?php

namespace UnicaenCode\Service\Traits;

use UnicaenCode\Service\CodeGeneratorService;
use Common\Exception\RuntimeException;
use UnicaenCode\Util;

trait CodeGeneratorServiceAwareTrait
{
    /**
     * @var CodeGeneratorService
     */
    private $serviceCodeGenerator;



    /**
     *
     * @param CodeGeneratorService $serviceCodeGenerator
     *
     * @return self
     */
    public function setServiceCodeGenerator(CodeGeneratorService $serviceCodeGenerator)
    {
        $this->serviceCodeGenerator = $serviceCodeGenerator;
        
        return $this;
    }



    /**
     *
     * @return CodeGeneratorService
     * @throws \Common\Exception\RuntimeException
     */
    public function getServiceCodeGenerator()
    {
        return $this->serviceCodeGenerator;
    }

}