# UnicaenCode

## Description

UnicaenCode est un module de la biliothèque Unicaen, dédiée au
développement avec Laminas. Le module est intégré à la
[Laminas Developer Toolbar](https://github.com/laminas/laminas-developer-tools), qu'il conviendra
d'installer préalablement pour un usage optimal 8-).

Son but est de fournir un ensemble de services facilitant le
développement d'applications. Parmis ces outils :

  - la coloration syntaxique de code source affichée sur une page HTML, 
  - des mécanismes avancés d'introspection de code source, 
  - un système de templates permettant d'auto-générer des fichiers de
    code source dont l'écriture est rébarbative (certains traits,
    interfaces, etc.).
  - Enfin, UnicaenCode propose un système de bac à sable où, au sein de
    son application, on peut exécuter des morceaux de code pour tester
    diverses fonctionnalités.

## Installation

### - 1 - Composer 

Ajouter la dépendance à votre fichier composer.json :
`composer require "unicaen/code" : "master",`

### - 2 - Configuration

Ajouter les deux fichiers de configuration dans votre /config/autoload en supprimant les ".dist":
[laminas-developer-tools.php](../config/laminas-developer-tools.php.dist)
[unicaen-code.global](../config/unicaen-code.global.php.dist)

UnicaenCode est destiné au développement d'application et n'a pas
vocation à être déployé sur un serveur de production. Aussi, plutôt que
de simplement déclarer le module, il est préférable de faire comme suit
:

### - 3 - Modules

Les developer tools et UnicaenCode sont tous deux des modules qu'il faut ajouter à la liste de vos modules chargés.
Dans l'idéal, il ne seront chargés qu'en mode développement.

Dans le fichier de configuration de l'application
(`application.config.php`), ajouter

``` php
$env = getenv('APP_ENV') ?: 'production';
if ( 'development' == $env ) {
    $modules[] = 'Laminas\DeveloperTools';
    $modules[] = 'UnicaenCode';
}
```

Ainsi, le module ne sera lancé que si l'application est sur un serveur
de développement. Pour rappel, la variable APP\_ENV se configure dans le
fichier *.htaccess* ou bien dans le *virtualHost* correspondant à votre
application (`SetEnv "APP_ENV" "development"`).

### - 4 - Mise en place des répertoires spécifiques

Créez ensuite à la racine de votre projet (ou ailleurs en adaptant votre configuration) un répertoire `code`,
puis à l'intérieur un sous-répertoire `template`.


# Utilisation

## Mode Bac à sable

Le mode Bac à sable permet d'exécuter de tester des fonctionnalités en
cours de développement. Concrètement, il suffit d'ajouter un fichier php
au répertoire `/code` (cf. paramètre `view-dir` de la configuration).

Ce fichier est une vue qui sera accessible via le menu Unicaen de la
Developer Toolbar : ![](/develop/unicaen2/laminas-uc.png)

### Variables

Dans ces scripts qui sont en fait des vues du modèle MVC, certaines
variables présentées par le bloc de phpDoc suivant :

``` php
/**
 * @var $this       \Application\View\Renderer\PhpRenderer
 * @var $container  \Psr\Container\ContainerInterface
 */
```

\<WRAP center round tip 80%\> Ce bloc pourra être écrit en entête de
chaque fichier pour que l'auto-complétion soit activée... \</WRAP\>

## Util

La classe `Util` comporte un certains nombre de méthodes statiques
permettant de réaliser, entres autres, les opérations suivantes :

### Util::sqlLog($display=true)

A partir de l'éxécution de cette méthode, toutes les requêtes SQL
exécutées par Doctrine sont comptabilisées et affichées si désiré.
Cela permet, par exemple, de connaître le nombre de requêtes générées
afin de les réduire ou les optimiser.

### Util::sqlCount

permet d'afficher le nombre de requêtes exécutées depuis l'appel à
sqlLog.

### Util::\*Dump (\* = php, sql, css, javascript, html ou xml)

permet d'afficher avec coloration syntaxique du code transmis sous forme
de chaîne de caractères.

\<WRAP center round tip 80%\> Petit bonus : le SQL est également indenté
car Doctrine génère des requêtes mono-lignes. De plus, sqlDump accepte
un `Doctrine\ORM\QueryBuilder` comme paramètre d'entrée\!\! \</WRAP\>

### Util::varDump

est une variante de var_dump, à ceci prêt qu'elle affiche les objets de
manière moins verbeuse.

## Introspection

Le service d'introspection est accessible de la manière suivante :

``` php
$sIntrospection = Util::introspection();
```

Il permet de lister les services d'un projet selon certains critères
ainsi que les entités Doctrine.

## Générateur de code

Le générateur de code est accessible de la manière suivante :

``` php
$sCodeGenerator = Util::codeGenerator();
```

Il permet de générer du code à partir d'un ensemble de paramètres passés sous forme
de tableau associatif, retravaillés à l'aide de Generators et mis en forme à l'aide de patrons (templates) disponibles
dans le dossier `/code/template`.

Donc on a :
 - des paramètres à passer à un générateur (en présisant dans la parametre "generator" le nom de notre générateur).
 - La générateur se charge de calculer des variables (qui sont aussi des paramètres) à partir de ces paramètres
 - ces variables sont ensuite injectés dans le template, ce qui retourne un code source sous forme de chaîne de caractères.
 - le rendu peut être soit affiché (echo), soit écrit sur le disque dur (write)

Vous pourrez créer vos propres générateurs.
Mais leur usage n'est pas obligatoire.

Voici maintenant un petit exemple de code qui va créer un service avec sa Factory : 

```php
$params = [
    // On utilise le générateur dédié aux services
    'generator'    => 'service',
    // On précise le nom complet de sa classe
    'class'        => 'Application\Service\ExempleService',
    // On affiche le résultat à l'écran ou on l'ajoute si on est en mode console
    'echo' => true,
    // on crée aussi une Factory
    'factory' => [
        // Et la factory ne sera pas dans un sous-dossier factory, mais dans le même répertoire que le service
        'subDir' => false,
    ],
];

// On lance la génération et on regarde à l'écran les deux fichiers produits...
\UnicaenCode\Util::codeGenerator()->generate($params);
```

A l'instar de la Factory, on aurait aussi pu générer un AwareTrait et une AwareInterface pour ce service.

Ce code utilise deux générateurs fournis par unicaenCode : 
[Service](../src/Generator/Service.php) et
[Factory](../src/Generator/Factory.php).
Et ces deux générateurs font appel aux templates
[Service](../code/template/Service.php) et
[Factory](../code/template/Factory.php).