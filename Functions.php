<?php

function sqlDump($sql)
{
    return \UnicaenCode\Util::sqlDump($sql);
}

function sqlResDump( $res ){
    return \UnicaenCode\Util::sqlResDump($res);
}

function phpDump($php)
{
    return \UnicaenCode\Util::phpDump($php, 'php');
}


function arrayDump(array $array)
{
    return \UnicaenCode\Util::arrayDump($array);
}


function javascriptDump($javascript)
{
    return \UnicaenCode\Util::javascriptDump($javascript, 'jquery');
}


function cssDump($css)
{
    return \UnicaenCode\Util::cssDump($css, 'css');
}


function htmlDump($html)
{
    return \UnicaenCode\Util::htmlDump($html, 'html5');
}


function xmlDump($xml)
{
    switch(true){
        case $xml instanceof \DOMDocument:
            $xml->formatOutput = true;
            $xml = $xml->saveXML();
        break;
        case $xml instanceof \DOMElement:
            $xml->ownerDocument->formatOutput = true;
            $xml = $xml->ownerDocument->saveXML($xml);
        break;
        case is_string($xml):
            $dd = new \DOMDocument();
            @$dd->loadXML($xml);
            $dd->formatOutput = true;
            $xml = $dd->saveXML();
        break;
    }

    return \UnicaenCode\Util::xmlDump($xml, 'xml');
}


function varDump($variable)
{
    return \UnicaenCode\Util::varDump($variable);
}

function dumpBacktrace()
{
    return \UnicaenCode\Util::dumpBacktrace();
}

function sqlLog($display=true)
{
    return \UnicaenCode\Util::sqlLog($display);
}