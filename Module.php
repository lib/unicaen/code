<?php

namespace UnicaenCode;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent;

include_once 'Functions.php';





/**
 *
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class Module implements ConfigProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        // on transmet pour qu'il soit accessible de partout en cas de besoins
        $container = $e->getApplication()->getServiceManager();
        Util::setContainer($container);
    }



    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }



    public function getConsoleUsage($console)
    {
        return [
            "Exécution de script",
            'UnicaenCode <script>' => "Lance un script UnicaenCode (ne pas ajouter .php)",
            'Jusqu\'à 9 paramètres peuvent être transmis.',
            'Première forme : une simple chaine de caractères. Là, le paramètre sera placé dans la variable $params de type array',
            'Deuxième forme : dans une variable dont le nom pourra être précisé sous la forme maVariable=maDonnee. Dans le script, echo($maVariable) affichera "maDonnee". La liste des variables pourra être récupérée dans le tableau $variables depuis le script'
        ];
    }
}