CHANGELOG
=========

6.3.0 (30/01/2025)
------------------

- Ajout d'un menu pour la route /unicaen-code qui avant renvoyait une erreur

6.2.3 (20/12/2024)
------------------

- [Fix] Retrait des derniers appels à Unicaen/Console

6.2.2 (22/11/2024)
------------------

- [Fix] Retrait de la dépendance à laminas/file, obsolète


6.2.1 (13/11/2024)
------------------

- [Fix] Lorsque Module.php est dans le répertoire src, tout refonctionne

6.2.0 (07/11/2024)
------------------

Utilisation de la console Symfony.


6.1.5 (07/10/2024)
------------------

- Nouveau générateur de commandes Symphony Console avec intégration par laminas-cli
- [Fix] Parsing des classes plus strict


6.1.4 (01/10/2024)
------------------

- Possibilité de modifier la commande de lancement d'UnicaenCode via paramétrage


6.1.3 (30/09/2024)
------------------

- Gestion arborescente du répertoire/menu "code"


6.1.2 (24/05/2024)
------------------

- [Fix] Plus d'erreur si les répertoires de code source ne sont pas trouvés; la liste de retour est simplement vide


6.1.1 (22/03/2024)
------------------

- Ajout d'une classe "geshi" pour tout bloc de code généré


6.1 (11/12/2023)
------------------

- Passage à PHP 8.2

6.0.10 (25/08/2023)
------------------

- Ajout d'arraydump pour afficher des tableaux en console comme en HTML


6.0.9 (13/07/2023)
------------------

- Ajout de l'item de menu pour accéder à la page des icônes d'UnicaenIcon

6.0.8 (22/03/2023)
------------------

- Adaptations à unicaenPrivileges

6.0.7 (22/03/2023)
------------------
- Adaptation des gabarits pour usage Privilege à la place de Auth

6.0.6 (21/03/2023)
------------------
- Adaptation des gabarits pour usage Authentification/Privilege/Utilisateur à la place de Auth

6.0.5 (02/12/2022)
------------------
- Adaptation à PHP8
- Suppression des paramétrages de config, trop complexes
- Ajout de la génération de configs à placer dans module.config.php

6.0.4 (15/12/2021)
------------------
- FormElementManagerV3Polyfill deprecated, remplacé par FormElementManager


6.0.3 (08/12/2021)
------------------
- Nouvelles corrections de bugs quite migration Laminas

6.0.2 (26/11/2021)
------------------

- Pas mal de bugs corrigés
- Prise en compte des PSR 0/4
- Meilleure logs à la génération

6.0.1 (24/11/2021)
------------------

- Normalisation Autoload pour PSR4


6.0 (23/11/2021)
------------------

- Migration vers Laminas


5.1 (15/11/2021)
------------------

- Correction d'un bug empêchant de générer les ClassMaps


5.0 (22/10/2021)
-------------------

- Refonte et simplification de la syntaxe, refactoring massif

4.5 (04/10/2021)
-------------------

- Nettoyage de dépendances inutiles
- Utilisation de la dernière version des ZFT (avant changement de nom)

4.4 (30/09/2021)
-------------------

- Correction de bug empêchant d'utiliser les formulaires en CLI


4.3 (29/09/2021)
-------------------

- Possibilité d'utiliser le générateur de code directement depuis la ligne de commande


4.2 (23/07/2021)
-------------------

- Ajout des triggers


4.1 (21/07/2021)
-------------------

- Ajout d'un générateur d'entités

4.0 (19/07/2021)
-------------------

- Refonte complète du dispositif avec une nouvelle syntaxe pour les paramètres & l'introduction des générateurs.

3.1 (12/07/2021)
-------------------
- Ajout d'un générateur de Classmap (pour optimiser les applis)

3.0.2 (12/11/2020)
-------------------
- Correction d'un bug de formatage du SQL à cause d'un service config pas trouvé
